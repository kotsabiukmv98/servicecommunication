cd ./ServiceCommunication.API
start cmd.exe @cmd /k "dotnet build && dotnet ef database update && dotnet run"
timeout 45
cd ../ServiceCommunication.Client
start cmd.exe @cmd /k "dotnet build && dotnet run"
cd ../ServiceCommunication.Worker
start cmd.exe @cmd /k "dotnet build && dotnet run"
@ECHO OFF
ECHO Congratulations! Applications were started:)
PAUSE