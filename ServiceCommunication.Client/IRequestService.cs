﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ServiceCommunication.Client
{
    interface IRequestService
    {
        Task Run();
    }
}
