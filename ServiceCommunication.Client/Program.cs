﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;
using System.Threading.Tasks;

namespace ServiceCommunication.Client
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.White;

            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            IConfiguration configuration = builder.Build();

            var serviceProvider = new ServiceCollection()
                .AddScoped<IRequestService, RequestService>()
                .AddSingleton<IConfiguration>(configuration)
                .BuildServiceProvider();

            var requestService = serviceProvider.GetService<IRequestService>();

            await requestService.Run();
        }
    }
}
