﻿using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Timers;

namespace ServiceCommunication.Client
{
    class RequestService : IRequestService
    {
        private readonly HttpClient _client;
        private readonly string _baseUrl;
        private HubConnection connection;
        private readonly IConfiguration _config;

        public RequestService(IConfiguration config)
        {
            _config = config;
            _baseUrl = _config.GetSection("BaseUrl").Value;
            _client = new HttpClient();
        }
        
        public async Task Run()
        {
            Configure();
            var key = new ConsoleKey();
            do
            {
                Console.Clear();
                Console.WriteLine("Visit SWAGGER page to see all available routes.");
                Console.WriteLine("Enter address for GET request and press 'Enter'...");
                Console.Write(_baseUrl);

                var input = Console.ReadLine();
                try
                {
                    var response = await _client.GetAsync(_baseUrl + input);
                    Console.WriteLine($"Response status: {response.StatusCode}");
                }
                catch (Exception ex)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error occurred during request");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine($"{ex.Message}");
                }
                Console.WriteLine($"Press 'ESC' for exit, 'SPACE' for starting delayed task or any other key to continue...");
                key = Console.ReadKey().Key;
                if (key == ConsoleKey.Spacebar)
                {
                    MarkRandomTaskWithDelay(1000);
                }

            } while (key != ConsoleKey.Escape);
        }

        private void MarkRandomTaskWithDelay(int delay)
        {
            var timer = new Timer(delay);
            timer.Elapsed += async (sender, e) => await HandlerTimer();
            timer.AutoReset = false;
            timer.Start();
        }

        private async Task HandlerTimer()
        {
            var client = new HttpClient();
            var response = await client.GetAsync(_baseUrl + "api/tasks");

            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine($"Request failed with status code {response.StatusCode}");
                return;
            }
            var tasks = await response.Content.ReadAsAsync<IList<Models.Task>>();
            var rndNumber = new Random().Next(0, tasks.Count);
            var markedTaskId = tasks[rndNumber].Id;
            response = await client.GetAsync(_baseUrl + $"api/tasks/marktaskasfinished/{markedTaskId}");

            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine($"Request failed with status code {response.StatusCode}");
                return;
            }
            Console.WriteLine($"Task with randomly id {markedTaskId} was successfully marked as finished");
        }

        private void Configure()
        {
            connection = new HubConnectionBuilder()
                .WithUrl($"{_baseUrl}logging")
                .Build();

            connection.Closed += (error) => Reconnect(error);

            connection.On<string>("ReceiveMessage", (message) => ReceiveMessage(message));
            connection.On<string>("ShowLogs", (message) => ShowLogs(message));

            connection.StartAsync();
        }

        private async Task Reconnect(Exception error)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"Connection was closed. Error: {error.Message}");
            await Task.Delay(new Random().Next(0, 5) * 1000);
            await connection.StartAsync();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"Connection status: {connection.State.ToString()}");
            Console.ForegroundColor = ConsoleColor.White;
        }

        private void ShowLogs(string message)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("LOGS:");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(message);
        }

        private void ReceiveMessage(string message)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Message from server: ");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(message);
        }
    }
}
