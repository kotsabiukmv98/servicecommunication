﻿using Newtonsoft.Json;
using System;

namespace ServiceCommunication.Client.Models
{
    class Task
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }

        [JsonProperty("state")]
        public int? TaskStateId { get; set; }
        public int? ProjectId { get; set; }
        public int? PerformerId { get; set; }
    }
}
