﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceCommunication.Core.Entity
{
    public class Team
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }

        [JsonIgnore]
        public List<Project> Projects { get; set; }
        [JsonIgnore]
        public List<User> Teammates { get; set; }
    }
}
