﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ServiceCommunication.Core.Entity
{
    public class TaskState
    {
        public int Id { get; set; }
        public string Value { get; set; }

        [JsonIgnore]
        public List<Task> Tasks { get; set; }
    }
}
