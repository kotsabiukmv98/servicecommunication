﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace ServiceCommunication.Core.Entity
{
    public class Project
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime Deadline { get; set; }
        public int? AuthorId { get; set; }
        public int? TeamId { get; set; }

        [JsonIgnore]
        public User Author { get; set; }
        [JsonIgnore]
        public Team Team { get; set; }
        [JsonIgnore]
        public List<Task> Tasks { get; set; }
    }
}
