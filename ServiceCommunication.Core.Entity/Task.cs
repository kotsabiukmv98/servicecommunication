﻿using Newtonsoft.Json;
using System;

namespace ServiceCommunication.Core.Entity
{
    public class Task
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }
        [JsonProperty("state")]
        public int? TaskStateId { get; set; }
        public int? ProjectId { get; set; }
        public int? PerformerId { get; set; }

        [JsonIgnore]
        public TaskState TaskState { get; set; }
        [JsonIgnore]
        public Project Project { get; set; }
        [JsonIgnore]
        public User Performer { get; set; }
    }
}
