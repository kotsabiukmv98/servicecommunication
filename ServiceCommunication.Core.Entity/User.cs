﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace ServiceCommunication.Core.Entity
{
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime RegisteredAt { get; set; }
        public int? TeamId { get; set; }
        public DateTime Birthday { get; set; }

        [JsonIgnore]
        public Team Team { get; set; }
        [JsonIgnore]
        public List<Task> Tasks { get; set; }
        [JsonIgnore]
        public List<Project> CreatedProjects { get; set; }
    }
}