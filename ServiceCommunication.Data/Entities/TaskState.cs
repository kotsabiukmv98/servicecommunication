﻿using System.Collections.Generic;

namespace ServiceCommunication.Data.Entities
{
    public class TaskState
    {
        public int Id { get; set; }
        public string Value { get; set; }

        public List<Task> Tasks { get; set; }
    }
}
