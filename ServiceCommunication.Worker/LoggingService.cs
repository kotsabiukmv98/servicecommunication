﻿using Microsoft.Extensions.Configuration;
using Nito.AsyncEx;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace ServiceCommunication.Worker
{
    class LoggingService : IDisposable, ILoggingService
    {
        private readonly IConfiguration _config;
        private EventingBasicConsumer _consumer;
        private IModel _channel;
        private IConnection _connection;
        private readonly string _folderPath;
        private readonly string _fileName;
        AsyncLock _asyncLock = new AsyncLock();

        public LoggingService(IConfiguration config)
        {
            _config = config;
            _folderPath = _config.GetSection("LoggingFolder").Value;
            _fileName = _config.GetSection("LoggingFileName").Value;
        }

        public void Dispose()
        {
            _connection?.Dispose();
            _channel?.Dispose();
        }

        public async Task RunAsync()
        {
            Console.Clear();
            Console.WriteLine("Worker has been started");

            await ConfigureAsync();
        }

        private async Task ConfigureAsync()
        {
            await CreateFileForLoggingAsync();
            var factory = new ConnectionFactory()
            {
                Uri = new Uri(_config.GetSection("Rabbit").Value)
            };

            _connection = factory.CreateConnection();

            _channel = _connection.CreateModel();
            _channel.ExchangeDeclare("ToDoExchange", ExchangeType.Direct);
            _channel.QueueDeclare("ToDoQueue",  true, false, false);
            _channel.QueueBind("ToDoQueue", "ToDoExchange", "key");

            _consumer = new EventingBasicConsumer(_channel);
            _consumer.Received += async (sender, args) 
                => await HandleQueueMessageAsync(sender, args);
            _channel.BasicConsume("ToDoQueue", false, _consumer);
        }

        private async Task CreateFileForLoggingAsync()
        {
            using (await _asyncLock.LockAsync())
            {
                Directory.CreateDirectory(_folderPath);
                using (FileStream fs = File.Create($"{_folderPath}/{_fileName}"));
            }
        }

        private async Task LogAsync(RequestInfo requestInfo)
        {
            using (await _asyncLock.LockAsync())
            {
                using (var sw = File.AppendText($"{_folderPath}/{_fileName}"))
                {
                    sw.WriteLine(requestInfo.DateTime.ToString());
                    sw.WriteLine(requestInfo.Message);
                }
            }
            Console.WriteLine("Request was logged");
        }

        private void SendMessageAsync(string message)
        {
            var factory = new ConnectionFactory()
            {
                Uri = new Uri(_config.GetSection("Rabbit").Value)
            };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.ExchangeDeclare("DoneExchange", ExchangeType.Direct);
                var body = Encoding.UTF8.GetBytes(message);
                channel.BasicPublish("DoneExchange", "key", null, body);
            }
        }

        private async Task HandleQueueMessageAsync(object sender, BasicDeliverEventArgs args)
        {
            try
            {
                var body = args.Body;
                var message = Encoding.UTF8.GetString(body);

                var requestInfo = new RequestInfo(message);
                await LogAsync(requestInfo);

                _channel.BasicAck(args.DeliveryTag, false);
                SendMessageAsync("Successfully handled queue message in worker");
            }
            catch (Exception ex)
            {
                SendMessageAsync($"Failed during handling a message. Error: {ex.Message}");
            }
        }
    }
}
