﻿using System;

namespace ServiceCommunication.Worker
{
    class RequestInfo
    {
        public RequestInfo(string message)
        {
            Message = message;
        }
        public DateTime DateTime { get; set; } = DateTime.Now;
        public string Message { get; set; }
    }
}
