﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.IO;
using System.Threading.Tasks;

namespace ServiceCommunication.Worker
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            IConfiguration configuration = builder.Build();

            var serviceProvider = new ServiceCollection()
                .AddScoped<ILoggingService, LoggingService>()
                .AddSingleton<IConfiguration>(configuration)
                .BuildServiceProvider();

            var loggingService =  serviceProvider.GetService<ILoggingService>();
            await loggingService.RunAsync();
        }
    }
}
