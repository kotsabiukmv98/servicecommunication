﻿using System.Threading.Tasks;

namespace ServiceCommunication.Worker
{
    interface ILoggingService
    {
        Task RunAsync();
    }
}
