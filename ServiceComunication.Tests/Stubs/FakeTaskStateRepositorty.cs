﻿using ServiceCommunication.Core.DomainService;
using ServiceCommunication.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace ServiceComunication.Tests.Stubs
{
    public class FakeTaskStateRepository : ITaskStateRepository
    {
        private ICollection<TaskState> _taskStates = new List<TaskState>();

        public Task<TaskState> Create(TaskState taskState)
        {
            _taskStates.Add(taskState);
            return Task.FromResult(taskState);
        }

        public System.Threading.Tasks.Task Delete(int id)
        {
            throw new NotImplementedException();
        }

        public Task<ICollection<TaskState>> ReadAll()
        {
            return Task.FromResult(_taskStates);
        }

        public Task<TaskState> ReadById(int id)
        {
            var result = _taskStates.FirstOrDefault(p => p.Id == id);
            return Task.FromResult(result);
        }

        public Task<TaskState> Update(TaskState taskStateUpdate)
        {
            throw new NotImplementedException();
        }
        public void Clear()
        {
            _taskStates.Clear();
        }
    }
}
