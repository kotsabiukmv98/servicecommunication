﻿using ServiceCommunication.Core.ApplicationService.Interfaces;

namespace ServiceComunication.Tests.Stubs
{
    class FakeQueueService : IQueueService
    {
        public bool SendMessage(string message)
        {
            return true;
        }
    }
}
