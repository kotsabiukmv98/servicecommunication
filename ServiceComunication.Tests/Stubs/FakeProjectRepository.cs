﻿using ServiceCommunication.Core.DomainService;
using ServiceCommunication.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace ServiceComunication.Tests.Stubs
{
    public class FakeProjectRepository : IProjectRepository
    {
        private ICollection<Project> _projects = new List<Project>();
        public Task<Project> Create(Project project)
        {
            _projects.Add(project);
            return Task.FromResult(project);
        }

        public Task Delete(int id)
        {
            throw new NotImplementedException();
        }

        public Task<ICollection<Project>> ReadAll()
        {
            return Task.FromResult(_projects);
        }

        public Task<Project> ReadById(int id)
        {
            var result = _projects.FirstOrDefault(p => p.Id == id);
            return Task.FromResult(result);
        }

        public Task<Project> Update(Project projectUpdate)
        {
            throw new NotImplementedException();
        }

        public void Clear()
        {
            _projects.Clear();
        }
    }
}
