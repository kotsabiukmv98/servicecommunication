﻿using ServiceCommunication.Core.DomainService;
using ServiceCommunication.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace ServiceComunication.Tests.Stubs
{
    public class FakeTaskRepository : ITaskRepository
    {
        private ICollection<ServiceCommunication.Core.Entity.Task> _tasks =
            new List<ServiceCommunication.Core.Entity.Task>();

        public Task<ServiceCommunication.Core.Entity.Task> Create(ServiceCommunication.Core.Entity.Task task)
        {
            _tasks.Add(task);
            return Task.FromResult(task);
        }

        public System.Threading.Tasks.Task Delete(int id)
        {
            throw new NotImplementedException();
        }

        public Task<ICollection<ServiceCommunication.Core.Entity.Task>> ReadAll()
        {
            return Task.FromResult(_tasks);
        }

        public Task<ServiceCommunication.Core.Entity.Task> ReadById(int id)
        {
            var result = _tasks.FirstOrDefault(t => t.Id == id);
            return Task.FromResult(result);
        }

        public Task<ServiceCommunication.Core.Entity.Task> Update(ServiceCommunication.Core.Entity.Task taskUpdate)
        {
            throw new NotImplementedException();
        }
        public void Clear()
        {
            _tasks.Clear();
        }
    }
}
