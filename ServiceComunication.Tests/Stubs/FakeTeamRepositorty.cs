﻿using ServiceCommunication.Core.DomainService;
using ServiceCommunication.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace ServiceComunication.Tests.Stubs
{
    class FakeTeamRepository : ITeamRepository
    {
        private ICollection<Team> _teams = new List<Team>();
        public Task<Team> Create(Team team)
        {
            _teams.Add(team);
            return Task.FromResult(team);
        }

        public System.Threading.Tasks.Task Delete(int id)
        {
            throw new NotImplementedException();
        }

        public Task<ICollection<Team>> ReadAll()
        {
            return Task.FromResult(_teams);
        }

        public Task<Team> ReadById(int id)
        {
            var result = _teams.FirstOrDefault(t => t.Id == id);
            return Task.FromResult(result);
        }

        public Task<Team> Update(Team teamUpdate)
        {
            throw new NotImplementedException();
        }
        public void Clear()
        {
            _teams.Clear();
        }
    }
}
