﻿using ServiceCommunication.Core.DomainService;
using ServiceCommunication.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace ServiceComunication.Tests.Stubs
{
    public class FakeUserRepository : IUserRepository
    {
        private ICollection<User> _users = new List<User>();

        public Task<User> Create(User customer)
        {
            _users.Add(customer);
            return Task.FromResult(customer);
        }

        public System.Threading.Tasks.Task Delete(int id)
        {
            throw new NotImplementedException();
        }

        public Task<ICollection<User>> ReadAll()
        {
            return Task.FromResult(_users);
        }

        public Task<User> ReadById(int id)
        {
            var result = _users.FirstOrDefault(u => u.Id == id);
            return Task.FromResult(result);
        }

        public Task<User> Update(User userUpdate)
        {
            throw new NotImplementedException();
        }
        public void Clear()
        {
            _users.Clear();
        }
    }
}
