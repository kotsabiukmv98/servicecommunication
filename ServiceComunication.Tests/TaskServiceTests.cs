﻿using AutoMapper;
using FakeItEasy;
using NUnit.Framework;
using ServiceCommunication.Core.ApplicationService.Interfaces;
using ServiceCommunication.Core.ApplicationService.Services;
using ServiceCommunication.Core.DomainService;
using ServiceCommunication.Core.Entity;
using System;

namespace ServiceComunication.Tests
{
    [TestFixture]
    class TaskServiceTests
    {
        [Test]
        public void MarkTaskAsFinished_When_mark_task_as_finished_Then_task_state_id_becomes_3()
        {
            // Arrange
            var queueService = A.Fake<IQueueService>();
            var taskRepository = A.Fake<ITaskRepository>();
            var mapper = A.Fake<IMapper>();
            var task = new Task() { Id = 10, CreatedAt = DateTime.Now, TaskStateId = 1 };
            A.CallTo(() => taskRepository.ReadById(A<int>._)).Returns(task);
            var taskService = new TaskService(taskRepository, queueService, mapper);

            // Act
            taskService.MarkTaskAsFinished(1).GetAwaiter().GetResult();

            // Assert
            Assert.That(task.TaskStateId, Is.EqualTo(3));
        }
    }
}
