﻿using NUnit.Framework;
using ServiceCommunication.Core.ApplicationService.Services;
using ServiceCommunication.Core.Entity;
using ServiceComunication.Tests.Stubs;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ServiceComunication.Tests
{
    [TestFixture]
    public class QueryServiceTests
    {
        private readonly FakeProjectRepository _fakeProjectRepository;
        private readonly FakeQueueService _fakeQueueService;
        private readonly FakeTaskRepository _fakeTaskRepository;
        private readonly FakeTaskStateRepository _fakeTaskStateRepository;
        private readonly FakeTeamRepository _fakeTeamRepository;
        private readonly FakeUserRepository _fakeUserRepository;
        private readonly QueryService _queryService;

        public QueryServiceTests()
        {
            _fakeProjectRepository = new FakeProjectRepository();
            _fakeQueueService = new FakeQueueService();
            _fakeTaskRepository = new FakeTaskRepository();
            _fakeTaskStateRepository = new FakeTaskStateRepository();
            _fakeTeamRepository = new FakeTeamRepository();
            _fakeUserRepository = new FakeUserRepository();
            _queryService = new QueryService(_fakeProjectRepository, _fakeTaskRepository,
                                           _fakeTaskStateRepository, _fakeTeamRepository,
                                           _fakeUserRepository, _fakeQueueService);
        }

        [SetUp]
        public void TestSetUp()
        {
            #region Task State Set Up

            _fakeTaskStateRepository.Create(new TaskState()
            { Id = 1, Value = "Created" });
            _fakeTaskStateRepository.Create(new TaskState()
            { Id = 2, Value = "Started" });
            _fakeTaskStateRepository.Create(new TaskState()
            { Id = 3, Value = "Finished" });
            _fakeTaskStateRepository.Create(new TaskState()
            { Id = 4, Value = "Canceled" });

            #endregion

            #region Task Set Up

            _fakeTaskRepository.Create(new Task()
            {
                Id = 1, CreatedAt = DateTime.Now, Description = "Here shoul be description", FinishedAt = DateTime.Now,
                Name = "Task with ID 1", PerformerId = 1, ProjectId = 1, TaskStateId = 4
            });
            _fakeTaskRepository.Create(new Task()
            {
                Id = 2, CreatedAt = DateTime.Now, Description = "Here shoul be description", FinishedAt = DateTime.Now,
                Name = "Task with ID 2", PerformerId = 2, ProjectId = 1, TaskStateId = 1
            });
            _fakeTaskRepository.Create(new Task()
            {
                Id = 3, CreatedAt = DateTime.Now, Description = "Here shoul be description", FinishedAt = DateTime.Now,
                Name = "Task with ID 3", PerformerId = 2, ProjectId = 2, TaskStateId = 2
            });
            _fakeTaskRepository.Create(new Task()
            {
                Id = 4, CreatedAt = DateTime.Now, Description = "Here shoul be description", FinishedAt = DateTime.Now,
                Name = "Task with ID 4", PerformerId = 1, ProjectId = 2, TaskStateId = 3
            });

            #endregion

            #region User Set Up

            _fakeUserRepository.Create(new User()
            {
                Id = 1, Birthday = new DateTime(1998, 1, 26), Email = "User email", FirstName = "First Name", LastName = "Last Name",
                RegisteredAt = DateTime.Now, TeamId = 1
            });

            _fakeUserRepository.Create(new User()
            {
                Id = 2, Birthday = DateTime.Now, Email = "User email", FirstName = "First Name", LastName = "Last Name",
                RegisteredAt = DateTime.Now, TeamId = 2
            });

            #endregion

            #region Team Set Up

            _fakeTeamRepository.Create(new Team()
            {
                Id = 1, CreatedAt = DateTime.Now, Name = "Team with ID 1"
            });

            _fakeTeamRepository.Create(new Team()
            {
                Id = 2, CreatedAt = DateTime.Now, Name = "Team with ID 2"
            });

            #endregion

            #region Project Set Up

            _fakeProjectRepository.Create(new Project()
            {
                Id = 1, AuthorId = 2, CreatedAt = DateTime.Now, Deadline = DateTime.Now, Name = "Projects with ID 2",
                Description = "Here should be project's description", TeamId = 2
            });

            _fakeProjectRepository.Create(new Project()
            {
                Id = 2, AuthorId = 1, CreatedAt = DateTime.Now, Deadline = DateTime.Now, Name = "Projects with ID 1",
                Description = "Here should be project's description", TeamId = 1
            });

            #endregion

        }

        [TearDown]
        public void TestTearDown()
        {
            _fakeProjectRepository.Clear();
            _fakeTaskRepository.Clear();
            _fakeTaskStateRepository.Clear();
            _fakeTeamRepository.Clear();
            _fakeUserRepository.Clear();
        }

        [Test]
        public void GetAmountOfUserTasksGroupedByProjects()
        {
            // Act
            var result = _queryService.GetAmountOfUserTasksGroupedByProjects(1).GetAwaiter().GetResult();

            // Assert
            Assert.That(result.Count(), Is.EqualTo(2));
            foreach (var p in result)
            {
                Assert.That(p.Count, Is.EqualTo(1));
            }
        }

        [Test]
        public void GetFirstMagicStruct()
        {
            // Act
            var result = _queryService.GetFirstMagicStruct(1).GetAwaiter().GetResult();

            // Assert
            Assert.That(result.AmountOfNotFinishedTasks, Is.EqualTo(1));
            Assert.That(result.AmountOfTasks, Is.EqualTo(1));
            Assert.That(result.LastProject.Id, Is.EqualTo(2));
            Assert.That(result.LongestTask.Id, Is.EqualTo(4));
            Assert.That(result.User.Id, Is.EqualTo(1));
        }

        [Test]
        public void GetSecondMagicStruct()
        {
            // Act
            var result = _queryService.GetSecondMagicStruct(1).GetAwaiter().GetResult();

            // Assert
            Assert.That(result.AmountOfUsers, Is.EqualTo(2));
            Assert.That(result.LongestTask.Id, Is.EqualTo(1));
            Assert.That(result.Project.Id, Is.EqualTo(1));
            Assert.That(result.ShortestTask.Id, Is.EqualTo(1));
        }

        [Test]
        public void GetSortedUsersWithTasks()
        {
            // Act
            var result = _queryService.GetSortedUsersWithTasks().GetAwaiter().GetResult();

            // Assert
            Assert.That(result.Count, Is.EqualTo(2));
            foreach (var p in result)
            {
                Assert.That(p.Tasks.Count, Is.EqualTo(2));
            }
        }

        [Test]
        public void GetTeamsWithUsersOlderThanTwelveYears()
        {
            // Act
            var result = _queryService.GetTeamsWithUsersOlderThanTwelveYears().GetAwaiter().GetResult();

            // Assert
            Assert.That(result.Count, Is.EqualTo(1));
            Assert.That(result.FirstOrDefault().Team.Id, Is.EqualTo(1));
            Assert.That(result.FirstOrDefault().Users.Count, Is.EqualTo(1));
            Assert.That(result.FirstOrDefault().Users.FirstOrDefault().Id, Is.EqualTo(1));
        }

        [Test]
        public void GetUserFinishedInCurrentYearTasks()
        {
            // Act
            var result = _queryService.GetUserFinishedInCurrentYearTasks(1).GetAwaiter().GetResult();

            // Assert
            Assert.That(result.Count, Is.EqualTo(1));
            Assert.That(result.FirstOrDefault().Id, Is.EqualTo(4));
            Assert.That(result.FirstOrDefault().Name, Is.EqualTo("Task with ID 4"));
        }

        [Test]
        public void GetUserTasksWithNameLessThanFortyFiveChars()
        {
            // Act
            var result = _queryService.GetUserTasksWithNameLessThanFortyFiveChars(1).GetAwaiter().GetResult();
            var shouldBeResult = new List<int>() { 1, 4 };

            // Assert
            Assert.That(result.Count, Is.EqualTo(2));
            Assert.That(result.Select(t => t.Id), Is.EquivalentTo(shouldBeResult));
        }
    }
}
