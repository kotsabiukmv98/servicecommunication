﻿using Microsoft.AspNetCore.Mvc.Testing;
using NUnit.Framework;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using ServiceCommunication.API;
using System.Collections.Generic;
using System;
using System.Net.Http.Headers;
using ServiceCommunication.Core.ApplicationService.Models;
using Newtonsoft.Json;
using System.Text;
using ServiceCommunication.Core.Entity;
using System.Linq;
using Task = System.Threading.Tasks.Task;

namespace ServiceComunication.Tests
{
    [TestFixture]
    class IntegrationTests
    {
        private APIWebApplicationFactory _factory;
        private HttpClient _client;

        [OneTimeSetUp]
        public void Init()
        {
            _factory = new APIWebApplicationFactory();
            _client = _factory.CreateClient();
        }

        [OneTimeTearDown]
        public void Cleanup()
        {
            _client.Dispose();
            _factory.Dispose();
        }

        [Test]
        public async Task CreateProject_When_project_create_Then_result_is_ok()
        {

            // Arrange
            var newProject = new NewProject()
            {
                Name = "Super Project",
                Description = "This is my Super-Puper projects",
                Deadline = DateTime.Now,
                AuthorId = 2,
                TeamId = 2
            };
            var requestBody = JsonConvert.SerializeObject(newProject);

            var requestMessage = new HttpRequestMessage(HttpMethod.Post, "api/projects");
            requestMessage.Content = new StringContent(requestBody, Encoding.UTF8, "application/json");

            // Act
            var response = await _client.SendAsync(requestMessage);

            // Assert
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
        }

        [Test]
        public async Task DeleteUser_When_user_delete_Then_find_the_same_user_should_be_not_found()
        {
            // Arrange
            var userId = 3;
            var requestMessage = new HttpRequestMessage(HttpMethod.Delete, $"api/users/{userId}");

            // Act
            var deleteResponse = await _client.SendAsync(requestMessage);
            var findResponse = await _client.GetAsync($"api/users/{userId}");

            // Assert
            Assert.That(deleteResponse.StatusCode, Is.EqualTo(HttpStatusCode.NoContent));
            Assert.That(findResponse.StatusCode, Is.EqualTo(HttpStatusCode.NotFound));
        }

        [Test]
        public async Task CreateTeam_When_team_create_Then_we_should_be_able_to_find_this_team()
        {
            // Arrange
            var newTeam = new NewTeam()
            {
                Name = "Super Team"
            };
            var requestBody = JsonConvert.SerializeObject(newTeam);

            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, "api/teams");
            requestMessage.Content = new StringContent(requestBody, Encoding.UTF8, "application/json");

            // Act
            var createNewTeamResponse = await _client.SendAsync(requestMessage);
            var getAllTeamsResponse = await _client.GetAsync("api/teams");
            var teams = await getAllTeamsResponse.Content.ReadAsAsync<List<Team>>();

            // Assert
            Assert.That(createNewTeamResponse.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(getAllTeamsResponse.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.NotNull(teams.FirstOrDefault(t => t.Name == "Super Team"));
        }

        [Test]
        public async Task GetDataHierarchically_Returns_Status_Code_Ok()
        {
            // Act
            var response = await _client.GetAsync("api/query/GetDataHierarchically");

            // Assert
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
        }

        [Test]
        public async Task DeleteTask_When_task_delete_Then_find_the_same_user_should_be_not_found()
        {
            // Arrange
            var taskId = 2;
            var requestMessage = new HttpRequestMessage(HttpMethod.Delete, $"api/tasks/{taskId}");

            // Act
            var deleteResponse = await _client.SendAsync(requestMessage);
            var findResponse = await _client.GetAsync($"api/tasks/{taskId}");

            // Assert
            Assert.That(deleteResponse.StatusCode, Is.EqualTo(HttpStatusCode.NoContent));
            Assert.That(findResponse.StatusCode, Is.EqualTo(HttpStatusCode.NotFound));
        }
    }
}