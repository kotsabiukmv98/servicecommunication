﻿using AutoMapper;
using FakeItEasy;
using NUnit.Framework;
using ServiceCommunication.Core.ApplicationService.Interfaces;
using ServiceCommunication.Core.ApplicationService.Models;
using ServiceCommunication.Core.ApplicationService.Services;
using ServiceCommunication.Core.DomainService;
using ServiceCommunication.Core.Entity;
using System;

namespace ServiceComunication.Tests
{
    [TestFixture]
    class UserServiceTests
    {
        [Test]
        public void AddUserToTeam_When_add_user_to_team_Then_team_id_become_to_new_value()
        {
            var queueService = A.Fake<IQueueService>();
            var userRepository = A.Fake<IUserRepository>();
            var mapper = A.Fake<IMapper>();
            var user = new User()
            {
                Id = 1,
                TeamId = null,
                FirstName = "Mykola"
            };
            A.CallTo(() => userRepository.ReadById(A<int>._)).Returns(user);
            var userService = new UserService(userRepository, queueService, mapper);

            // Act
            userService.AddUserToTeam(1, 2).GetAwaiter().GetResult();

            // Assert
            Assert.That(user.TeamId, Is.EqualTo(2));
        }

        [Test]
        public void CreateUser_When_create_customer_Then_map_and_create_should_be_valled_ones()
        {
            // Arrange
            var queueService = A.Fake<IQueueService>();
            var userRepository = A.Fake<IUserRepository>();
            var customer = new User()
            {
                Id = 1,
                FirstName = "Dmytro",
                LastName = "Dedorive",
                Email = "dmytro@email.com"
            };
            A.CallTo(() => userRepository.Create(A<User>._)).Returns(customer);

            var mapper = A.Fake<IMapper>();
            A.CallTo(() => mapper.Map<NewUser, User>(A<NewUser>._)).Returns(null);
            var userService = new UserService(userRepository, queueService, mapper);

            // Act
            var user = userService.CreateUser(new NewUser()
            {
                Birthday = new DateTime(1996, 10, 10),
                Email = "myemail@email.com",
                FirstName = "Kolia",
                LastName = "Petrov",
                TeamId = 1
            }).GetAwaiter().GetResult();

            // Assert
            A.CallTo(() => mapper.Map<NewUser, User>(A<NewUser>._)).MustHaveHappened(1, Times.Exactly);
            A.CallTo(() => userRepository.Create(A<User>._)).MustHaveHappened(1, Times.Exactly);
            Assert.That(user.Id, Is.EqualTo(1));
            Assert.That(user.Email, Is.EqualTo("dmytro@email.com"));
        }
    }
}
