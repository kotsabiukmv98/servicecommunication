﻿using AutoMapper;
using ServiceCommunication.Core.ApplicationService.Interfaces;
using ServiceCommunication.Core.ApplicationService.Models;
using ServiceCommunication.Core.DomainService;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace ServiceCommunication.Core.ApplicationService.Services
{
    public class TaskService : ITaskService
    {
        readonly ITaskRepository _taskRepo;
        readonly IMapper _mapper;
        readonly IQueueService _queueService;

        public TaskService(ITaskRepository taskRepository,
                           IQueueService queueService,
                           IMapper mapper)
        {
            _taskRepo = taskRepository;
            _mapper = mapper;
            _queueService = queueService;
        }

        public async Task<Entity.Task> CreateTask(NewTask newTask)
        {
            _queueService.SendMessage($"Create task was triggered");

            var task = _mapper.Map<NewTask, Entity.Task>(newTask);
            if (task.TaskStateId == 3 || task.TaskStateId == 4)
                task.FinishedAt = DateTime.Now;

            return await _taskRepo.Create(task);
        }

        public async Task DeleteTask(int id)
        {
            _queueService.SendMessage($"Delete task was triggered");

            await _taskRepo.Delete(id);
        }

        public async Task<Entity.Task> FindTaskById(int id)
        {
            _queueService.SendMessage($"Find task by id was triggered");

            return await _taskRepo.ReadById(id);
        }

        public async Task<ICollection<Entity.Task>> GetAllTasks()
        {
            _queueService.SendMessage($"Get all tasks was triggered");

            return await _taskRepo.ReadAll();
        }

        public async Task MarkTaskAsFinished(int id)
        {
            var task = await _taskRepo.ReadById(id);
            if (task == null)
                return;
            task.TaskStateId = 3;
            await _taskRepo.Update(task);
        }

        public async Task<Entity.Task> UpdateTask(int id, NewTask newTaskUpdate)
        {
            _queueService.SendMessage($"Update task was triggered");

            var task = _mapper.Map<NewTask, Entity.Task>(newTaskUpdate);
            task.Id = id;

            if (task.TaskStateId == 3 || task.TaskStateId == 4)
                task.FinishedAt = DateTime.Now;

            return await _taskRepo.Update(task);
        }
    }
}
