﻿using AutoMapper;
using ServiceCommunication.Core.ApplicationService.Interfaces;
using ServiceCommunication.Core.ApplicationService.Models;
using ServiceCommunication.Core.DomainService;
using ServiceCommunication.Core.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace ServiceCommunication.Core.ApplicationService.Services
{
    public class TeamService : ITeamService
    {
        readonly ITeamRepository _teamRepo;
        readonly IMapper _mapper;
        readonly IQueueService _queueService;

        public TeamService(ITeamRepository teamRepository,
                           IQueueService queueService,
                           IMapper mapper)
        {
            _queueService = queueService;
            _teamRepo = teamRepository;
            _mapper = mapper;
        }

        public async Task<Team> CreateTeam(NewTeam newTeam)
        {
            _queueService.SendMessage($"Create team was triggered");

            var team = _mapper.Map<NewTeam, Team>(newTeam);
            return await _teamRepo.Create(team);
        }

        public async Task DeleteTeam(int id)
        {
            _queueService.SendMessage($"Delete team was triggered");

            await _teamRepo.Delete(id);
        }

        public async Task<Team> FindTeamById(int id)
        {
            _queueService.SendMessage($"Find team by id was triggered");

            return await _teamRepo.ReadById(id);
        }

        public async Task<ICollection<Team>> GetAllTeams()
        {
            _queueService.SendMessage($"Get all teams was triggered");

            return await _teamRepo.ReadAll();
        }

        public async Task<Team> UpdateTeam(int id, NewTeam teamUpdate)
        {
            _queueService.SendMessage($"Update team was triggered");

            var team = _mapper.Map<NewTeam, Team>(teamUpdate);
            team.Id = id;
            return await _teamRepo.Update(team);
        }
    }
}
