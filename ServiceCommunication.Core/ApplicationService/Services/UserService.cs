﻿using AutoMapper;
using ServiceCommunication.Core.ApplicationService.Interfaces;
using ServiceCommunication.Core.ApplicationService.Models;
using ServiceCommunication.Core.DomainService;
using ServiceCommunication.Core.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace ServiceCommunication.Core.ApplicationService.Services
{
    public class UserService : IUserService
    {
        readonly IUserRepository _userRepository;
        readonly IMapper _mapper;
        readonly IQueueService _queueService;

        public UserService(IUserRepository userRepository,
                           IQueueService queueService,
                           IMapper mapper)
        {
            _queueService = queueService;
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public async Task<User> CreateUser(NewUser newUser)
        {
            _queueService.SendMessage($"Create user was triggered");

            var user = _mapper.Map<NewUser, User>(newUser);
            return await _userRepository.Create(user);
        }

        public async Task DeleteUser(int id)
        {
            _queueService.SendMessage($"Delete user was triggered");

            await _userRepository.Delete(id);
        }

        public async Task<User> FindUserById(int id)
        {
            _queueService.SendMessage($"Find user by id was triggered");

            return await _userRepository.ReadById(id);
        }

        public async Task<ICollection<User>> GetAllUsers()
        {
            _queueService.SendMessage($"Get all users was triggered");

            return await _userRepository.ReadAll();
        }

        public async Task<User> UpdateUser(int Id, NewUser userUpdate)
        {
            _queueService.SendMessage($"Update user was triggered");

            var user = _mapper.Map<NewUser, User>(userUpdate);
            user.Id = Id;
            return await _userRepository.Update(user);
        }

        public async Task<User> AddUserToTeam(int userId, int teamId)
        {
            _queueService.SendMessage($"Add user with ID {userId} to team with ID {teamId} was triggered");

            var user = await _userRepository.ReadById(userId);
            if (user != null)
            {
                user.TeamId = teamId;
            }

            return user;
        }
    }
}
