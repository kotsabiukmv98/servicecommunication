﻿using ServiceCommunication.Core.ApplicationService.Interfaces;
using ServiceCommunication.Core.ApplicationService.Models;
using ServiceCommunication.Core.DomainService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace ServiceCommunication.Core.ApplicationService.Services
{
    public class QueryService : IQueryService
    {
        readonly IProjectRepository _projectRepository;
        readonly ITaskRepository _taskRepository;
        readonly ITaskStateRepository _taskStateRepository;
        readonly ITeamRepository _teamRepository;
        readonly IUserRepository _userRepository;
        readonly IQueueService _queueService;

        public QueryService(IProjectRepository projectRepo, 
                            ITaskRepository taskRepository,
                            ITaskStateRepository taskStateRepository,
                            ITeamRepository teamRepository,
                            IUserRepository userRepository,
                            IQueueService queueService)
        {
            _projectRepository = projectRepo;
            _taskRepository = taskRepository;
            _taskStateRepository = taskStateRepository;
            _teamRepository = teamRepository;
            _userRepository = userRepository;
            _queueService = queueService;
        }

        //TODO: Consider changing List to ICollection here
        public async Task<List<ProjectInfo>> GetDataHierarchically()
        {
            _queueService.SendMessage($"Get data hierarchically was triggered");
            var projects =  _projectRepository.ReadAll();
            var tasks = _taskRepository.ReadAll();
            var users = _userRepository.ReadAll();
            var teams = _teamRepository.ReadAll();

            await Task.WhenAll(projects, tasks, users, teams);

            var result = projects.Result.AsParallel().Select(p => new ProjectInfo
            {
                Project = p,
                TasksInfo = tasks.Result.Where(t => t.ProjectId == p.Id).Select(t => new TaskInfo
                {
                    Task = t,
                    Performer = users.Result.FirstOrDefault(u => u.Id == t.PerformerId)
                }),
                Author = users.Result.FirstOrDefault(u => u.Id == p.AuthorId),
                Team = teams.Result.FirstOrDefault(t => t.Id == p.TeamId)
            }).ToList();

            return result;
        }

        public async Task<List<GroupedTasks>> GetAmountOfUserTasksGroupedByProjects(int userId)
        {
            _queueService.SendMessage($"Get amount of user's tasks grouped by projects was triggered");

            var tasks = _taskRepository.ReadAll();
            var projects = _projectRepository.ReadAll();

            await Task.WhenAll(tasks, projects);

            
            var result = projects.Result.GroupJoin(tasks.Result.Where(t => t.PerformerId == userId),
                p => p.Id,
                t => t.ProjectId,
                (project, tsks) => new GroupedTasks
                {
                    Project = project,
                    Count = tsks.Count()
                }).Where(g => g.Count > 0).ToList();

            return result;
        }

        public async Task<List<Core.Entity.Task>> GetUserTasksWithNameLessThanFortyFiveChars(int userId)
        {
            _queueService.SendMessage($"Get user tasks with name less than forty five characters was triggered");

            var tasks = await _taskRepository.ReadAll();
            var result = tasks.AsParallel()
                .Where(t => t.PerformerId == userId && t.Name.Length < 45)
                .ToList();

            return result;
        }

        public async Task<List<TaskName>> GetUserFinishedInCurrentYearTasks(int userId)
        {
            _queueService.SendMessage($"Get user finished in current year tasks was triggered");

            var taskStates = _taskStateRepository.ReadAll();
            var tasks = _taskRepository.ReadAll();

            await Task.WhenAll(tasks, taskStates);

            Func<Core.Entity.Task, bool> predicate = task =>
            {
                int currentYear = DateTime.Now.Year;
                return
                    task.PerformerId == userId &&
                    taskStates.Result.FirstOrDefault(state => state.Id == task.TaskStateId).Value == "Finished" &&
                    task.FinishedAt.Year == currentYear;
            };
            var result = tasks.Result.AsParallel()
                    .Where(predicate)
                    .Select(t => new TaskName
                    {
                        Id = t.Id,
                        Name = t.Name
                    })
                    .ToList();

            return result;
        }

        public async Task<List<TeamWithUsers>> GetTeamsWithUsersOlderThanTwelveYears()
        {
            _queueService.SendMessage($"Get teams with users older than twelve years was triggered");
            var teams = _teamRepository.ReadAll();
            var users = _userRepository.ReadAll();

            await Task.WhenAll(teams, users);

            

            var result = (from team in teams.Result
                          join user in users.Result.Where(u => DateTime.Now.Year - u.Birthday.Year > 12)
                          on team.Id equals user.TeamId
                          group new { team, user } by team into grp 
                          select new TeamWithUsers
                          {
                              Team = grp.Key,
                              Users = grp.Select(q => q.user).ToList()
                          } 
                         ).ToList();

            return result;
        }

        public async Task<List<UserWithTasks>> GetSortedUsersWithTasks()
        {
            _queueService.SendMessage($"Get sorted users with tasks was triggered");
            var users = _userRepository.ReadAll();
            var tasks = _taskRepository.ReadAll();

            await Task.WhenAll(users, tasks);

            var result = users.Result
                .OrderBy(u => u.FirstName)
                .Select(u => new UserWithTasks
                {
                    User = u,
                    Tasks = tasks.Result
                        .Where(t => t.PerformerId == u.Id)
                        .OrderByDescending(t => t.Name.Length)
                        .ToList()
                }).ToList();

            return result;
        }

        public async Task<FirstMagicStruct> GetFirstMagicStruct(int userId)
        {
            _queueService.SendMessage($"Get first magic structure was triggered");

            var user = _userRepository.ReadById(userId);
            var taskStates = _taskStateRepository.ReadAll();

            await Task.WhenAll(user, taskStates);

            var lastProject = (await _projectRepository.ReadAll())
                    .Where(p => user.Result.Id == p.AuthorId)
                    .OrderByDescending(p => p.CreatedAt)
                    .FirstOrDefault();

            var tasks = (await _taskRepository.ReadAll())
                .Where(t => user.Result.Id == t.PerformerId);

            var result = new FirstMagicStruct
            {
                User = user.Result,
                LastProject = lastProject,
                AmountOfTasks = tasks
                                 .Where(t => t.ProjectId == lastProject?.Id)
                                 .Count(),
                AmountOfNotFinishedTasks = tasks
                                 .Where(t => taskStates.Result.FirstOrDefault(ts => ts.Id == t.TaskStateId).Value != "Finished")
                                 .Count(),
                LongestTask = tasks
                                 .OrderByDescending(t => t.CreatedAt - t.FinishedAt)
                                 .FirstOrDefault()
            };
            return result;
        }

        public async Task<SecondMagicStruct> GetSecondMagicStruct(int projectId)
        {
            _queueService.SendMessage($"Get second magic structure was triggered");

            var projects = _projectRepository.ReadAll();
            var tasks = _taskRepository.ReadAll();
            var users = _userRepository.ReadAll();
            var teams = _teamRepository.ReadAll();

            await Task.WhenAll(projects, tasks, users, teams);

            var result = (from project in projects.Result
                         where project.Id == projectId
                         let tsks = tasks.Result.Where(t => t.ProjectId == project.Id)
                         let team = teams.Result.FirstOrDefault(t => t.Id == project.TeamId)
                         select new SecondMagicStruct
                         {
                             Project = project,
                             LongestTask = tsks
                                .OrderByDescending(t => t.Description.Length).FirstOrDefault(),
                             ShortestTask = tsks
                                 .OrderBy(t => t.Name).FirstOrDefault(),
                             AmountOfUsers = projects.Result.GroupJoin(tasks.Result, p => p.Id, t => t.ProjectId, (project, task) => new
                             {
                                 project.Id,
                                 project.Description,
                                 TaskCount = task.Count()
                             }).Where(p => (p.TaskCount < 3 && p.TaskCount != 0) || (p.Description.Length > 25))
                                             .Select(p => tasks.Result.FirstOrDefault(t => t.ProjectId == p.Id))
                                             .Select(t => users.Result.FirstOrDefault(u => u.Id == t?.PerformerId))
                                             .GroupBy(u => u?.Id)
                                             .Count()
                         }).FirstOrDefault();

            return result;
        }
    }
}
