﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using ServiceCommunication.Core.ApplicationService.Hubs;
using ServiceCommunication.Core.ApplicationService.Interfaces;
using System;
using System.Text;

namespace ServiceCommunication.Core.ApplicationService.Services
{
    public class QueueService : IQueueService, IDisposable
    {
        private readonly IConfiguration _config;
        private EventingBasicConsumer _consumer;
        private IModel _channel;
        private IConnection _connection;
        private readonly IHubContext<ClientHub> _clientHub;

        public QueueService(IConfiguration config,
                            IHubContext<ClientHub> hubContext)
        {
            _config = config;
            _clientHub = hubContext;
            Configure();
        }

        public void Dispose()
        {
            _connection?.Dispose();
            _channel?.Dispose();
        }

        public bool SendMessage(string message)
        {
            var factory = new ConnectionFactory()
            {
                Uri = new Uri(_config.GetSection("Rabbit").Value)
            };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.ExchangeDeclare("ToDoExchange", ExchangeType.Direct);

                var body = Encoding.UTF8.GetBytes(message);

                channel.BasicPublish("ToDoExchange", "key", null, body);

                return true;
            }
        }
        private void Configure()
        {
            var factory = new ConnectionFactory()
            {
                Uri = new Uri(_config.GetSection("Rabbit").Value)
            };

            _connection = factory.CreateConnection();

            _channel = _connection.CreateModel();
            _channel.ExchangeDeclare("DoneExchange", ExchangeType.Direct);
            _channel.QueueDeclare("DoneQueue", true, false, false);
            _channel.QueueBind("DoneQueue", "DoneExchange", "key");

            _consumer = new EventingBasicConsumer(_channel);
            _consumer.Received += HandleQueueMessage;
            _channel.BasicConsume("DoneQueue", false, _consumer);
        }

        private void HandleQueueMessage(object sender, BasicDeliverEventArgs args)
        {
            var body = args.Body;
            var message = Encoding.UTF8.GetString(body);

            _clientHub.Clients.All.SendAsync("ReceiveMessage", message);
            _channel.BasicAck(args.DeliveryTag, false);

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Handled queue message");
            Console.ForegroundColor = ConsoleColor.Green;
        }
    }
}
