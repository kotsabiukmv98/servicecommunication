﻿using AutoMapper;
using ServiceCommunication.Core.ApplicationService.Interfaces;
using ServiceCommunication.Core.ApplicationService.Models;
using ServiceCommunication.Core.DomainService;
using ServiceCommunication.Core.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace ServiceCommunication.Core.ApplicationService.Services
{
    public class ProjectService : IProjectService
    {
        readonly IProjectRepository _projectRepo;
        readonly IMapper _mapper;
        readonly IQueueService _queueService;

        public ProjectService(IProjectRepository projectRepository,
                              IQueueService queueService,
                              IMapper mapper)
        {
            _mapper = mapper;
            _queueService = queueService;
            _projectRepo = projectRepository;
        }

        public async Task<Project> CreateProject(NewProject newProject)
        {
            var project = _mapper.Map<NewProject, Project>(newProject);
            _queueService.SendMessage($"Create project was triggered");
            return await _projectRepo.Create(project);
        }

        public async Task DeleteProject(int id)
        {
            _queueService.SendMessage($"Delete project was triggered");
            await _projectRepo.Delete(id);
        }

        public async Task<Project> FindProjectById(int id)
        {
            _queueService.SendMessage($"Find project by id was triggered");
            return  await _projectRepo.ReadById(id);
        }

        public async Task<ICollection<Project>> GetAllProjects()
        {
            _queueService.SendMessage($"Get all project was triggered");
            return await _projectRepo.ReadAll();
        }

        public async Task<Project> UpdateProject(int id, NewProject projectUpdate)
        {
            _queueService.SendMessage($"Update project was triggered");
            var project = _mapper.Map<NewProject, Project>(projectUpdate);
            project.Id = id;
            return await _projectRepo.Update(project);
        }
    }
}
