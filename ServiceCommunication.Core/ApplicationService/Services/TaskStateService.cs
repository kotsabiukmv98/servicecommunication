﻿using AutoMapper;
using ServiceCommunication.Core.ApplicationService.Interfaces;
using ServiceCommunication.Core.ApplicationService.Models;
using ServiceCommunication.Core.DomainService;
using ServiceCommunication.Core.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace ServiceCommunication.Core.ApplicationService.Services
{
    public class TaskStateService : ITaskStateService
    {
        readonly ITaskStateRepository _taskStateRepo;
        readonly IMapper _mapper;
        readonly IQueueService _queueService;

        public TaskStateService(ITaskStateRepository taskStateRepository,
                                IQueueService queueService,
                                IMapper mapper)
        {
            _queueService = queueService;
            _taskStateRepo = taskStateRepository;
            _mapper = mapper;
        }

        public async Task<TaskState> CreateTaskState(NewTaskState newTaskState)
        {
            _queueService.SendMessage($"Create task state was triggered");

            var taskState = _mapper.Map<NewTaskState, TaskState>(newTaskState);
            return await _taskStateRepo.Create(taskState);
        }

        public async Task DeleteTaskState(int id)
        {
            _queueService.SendMessage($"Delete task state was triggered");
            await _taskStateRepo.Delete(id);
        }

        public async Task<TaskState> FindTaskStateById(int id)
        {
            _queueService.SendMessage($"Find task state by id was triggered");
            return await _taskStateRepo.ReadById(id);
        }

        public async Task<ICollection<TaskState>> GetAllTaskStates()
        {
            _queueService.SendMessage($"Get all task states was triggered");

            return await _taskStateRepo.ReadAll();
        }

        public async Task<TaskState> UpdateTaskState(int id, NewTaskState taskStateUpdate)
        {
            _queueService.SendMessage($"Update task state was triggered");

            var task = _mapper.Map<NewTaskState, TaskState>(taskStateUpdate);
            task.Id = id;
            return await _taskStateRepo.Update(task);
        }
    }
}
