﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using ServiceCommunication.Core.ApplicationService.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using ServiceCommunication.Core.ApplicationService.Models;
using ServiceCommunication.Core.ApplicationService.Hubs;
using System.Threading.Tasks;

namespace ServiceCommunication.Core.ApplicationService.Services
{
    public class LogService : ILogService
    {
        private readonly IHubContext<ClientHub> _clientHub;
        private readonly string _logsFilePath;

        public LogService(IHubContext<ClientHub> hubContext,
                              IConfiguration configure)
        {
            _clientHub = hubContext;
            _logsFilePath = configure.GetSection("LogsFilePath").Value;
        }
        public async Task SendLogs()
        {
            try
            {
                var requests = new List<RequestInfo>();
                using (StreamReader sr = new StreamReader(_logsFilePath))
                {
                    string dateTime;
                    string message;
                    while ((dateTime = sr.ReadLine()) != null)
                    {
                        message = sr.ReadLine();
                        requests.Add(new RequestInfo(message, dateTime));
                    }
                }
                var sb = new StringBuilder("");
                if (requests.Count == 0)
                {
                    await _clientHub.Clients.All.SendAsync("ShowLogs", "No records!");
                    return;
                }
                foreach (var request in requests.OrderByDescending(r => r.DateTime))
                {
                    sb.Append($"{request.ToString()}\n");
                }

                await _clientHub.Clients.All.SendAsync("ShowLogs", sb.ToString());
                return;
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ex.Message);
                Console.ForegroundColor = ConsoleColor.White;
            }
        }
    }
}
