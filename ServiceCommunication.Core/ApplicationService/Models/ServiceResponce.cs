﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceCommunication.Core.ApplicationService.Models
{
    public class ServiceResponce
    {
        public bool Succeeded { get; set; }

        public string Message { get; set; } = "";

        public string Responce { get; set; }
    }
}
