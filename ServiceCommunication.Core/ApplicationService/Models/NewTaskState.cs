﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceCommunication.Core.ApplicationService.Models
{
    public class NewTaskState
    {
        public string Value { get; set; }
    }
}
