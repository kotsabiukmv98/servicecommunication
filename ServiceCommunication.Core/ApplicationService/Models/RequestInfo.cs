﻿using System;

namespace ServiceCommunication.Core.ApplicationService.Models
{
    public class RequestInfo
    {
        public RequestInfo(string message,
                           string dateTime)
        {
            Message = message;
            DateTime = DateTime.Parse(dateTime);
        }
        public DateTime DateTime { get; set; }
        public string Message { get; set; }

        public override string ToString()
        {
            return $"{DateTime.ToString()}\n{Message}";
        }
    }
}
