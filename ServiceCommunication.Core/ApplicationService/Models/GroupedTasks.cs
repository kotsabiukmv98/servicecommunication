﻿using ServiceCommunication.Core.Entity;

namespace ServiceCommunication.Core.ApplicationService.Models
{
    public class GroupedTasks
    {
        public Project  Project { get; set; }

        public int Count { get; set; }
    }
}
