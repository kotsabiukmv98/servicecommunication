﻿using ServiceCommunication.Core.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceCommunication.Core.ApplicationService.Models
{
    public class SecondMagicStruct
    {
        public Project Project { get; set; }
        public Task LongestTask { get; set; }
        public Task ShortestTask { get; set; }
        public int AmountOfUsers { get; set; }
    }
}
