﻿using ServiceCommunication.Core.Entity;

namespace ServiceCommunication.Core.ApplicationService.Models
{
    public class FirstMagicStruct
    {
        public User User { get; set; }
        public Project LastProject { get; set; }
        public int AmountOfTasks { get; set; }
        public int AmountOfNotFinishedTasks { get; set; }
        public Task LongestTask { get; set; }
    }
}
