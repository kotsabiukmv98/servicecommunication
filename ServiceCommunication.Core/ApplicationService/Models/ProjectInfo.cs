﻿using ServiceCommunication.Core.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceCommunication.Core.ApplicationService.Models
{
    public class ProjectInfo
    {
        public Project Project {get; set;}
        public IEnumerable<TaskInfo> TasksInfo {get; set;}
        public User Author {get; set;}
        public Team Team {get; set;}
    }
}
