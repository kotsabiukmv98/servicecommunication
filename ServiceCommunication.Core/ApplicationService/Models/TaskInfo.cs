﻿using ServiceCommunication.Core.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceCommunication.Core.ApplicationService.Models
{
    public class TaskInfo
    {
        public Task Task { get; set; }
        public User Performer { get; set; }
    }
}
