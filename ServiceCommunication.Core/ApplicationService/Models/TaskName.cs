﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceCommunication.Core.ApplicationService.Models
{
    public class TaskName
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
