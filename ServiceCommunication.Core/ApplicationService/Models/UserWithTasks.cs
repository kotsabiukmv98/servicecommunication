﻿using ServiceCommunication.Core.Entity;
using System.Collections.Generic;

namespace ServiceCommunication.Core.ApplicationService.Models
{
    public class UserWithTasks
    {
        public User User { get; set; }
        public List<Task> Tasks {get; set; }
    }
}
