﻿using ServiceCommunication.Core.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceCommunication.Core.ApplicationService.Models
{
    public class TeamWithUsers
    {
        public Team Team { get; set; }
        public List<User> Users { get; set; }
    }
}
