﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceCommunication.Core.ApplicationService.Models
{
    public class NewTask
    {
        public string Name { get; set; }
        public string Description { get; set; }
        [JsonProperty("state")]
        public int TaskStateId { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
    }
}
