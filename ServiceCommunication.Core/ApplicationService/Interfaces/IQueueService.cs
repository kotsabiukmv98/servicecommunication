﻿namespace ServiceCommunication.Core.ApplicationService.Interfaces
{
    public interface IQueueService
    {
        bool SendMessage(string message);
    }
}
