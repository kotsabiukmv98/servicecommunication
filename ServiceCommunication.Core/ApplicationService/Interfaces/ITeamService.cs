﻿using ServiceCommunication.Core.ApplicationService.Models;
using ServiceCommunication.Core.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace ServiceCommunication.Core.ApplicationService.Interfaces
{
    public interface ITeamService
    {
        Task<Team> CreateTeam(NewTeam team);
        Task<Team> FindTeamById(int id);
        Task<ICollection<Team>> GetAllTeams();
        Task<Team> UpdateTeam(int id, NewTeam teamUpdate);
        Task DeleteTeam(int id);
    }
}