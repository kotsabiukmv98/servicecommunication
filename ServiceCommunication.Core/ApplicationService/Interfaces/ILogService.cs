﻿using System.Threading.Tasks;

namespace ServiceCommunication.Core.ApplicationService.Interfaces
{
    public interface ILogService
    {
        Task SendLogs();
    }
}
