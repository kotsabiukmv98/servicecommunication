﻿using ServiceCommunication.Core.ApplicationService.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ServiceCommunication.Core.ApplicationService.Interfaces
{
    public interface IQueryService
    {
        Task<List<ProjectInfo>> GetDataHierarchically();
        Task<List<GroupedTasks>>  GetAmountOfUserTasksGroupedByProjects(int userId);
        Task<List<Entity.Task>> GetUserTasksWithNameLessThanFortyFiveChars(int userId);
        Task<List<TeamWithUsers>> GetTeamsWithUsersOlderThanTwelveYears();
        Task<List<UserWithTasks>> GetSortedUsersWithTasks();
        Task<List<TaskName>> GetUserFinishedInCurrentYearTasks(int userId);
        Task<FirstMagicStruct> GetFirstMagicStruct(int userId);
        Task<SecondMagicStruct> GetSecondMagicStruct(int projectId);
    }
}