﻿using ServiceCommunication.Core.ApplicationService.Models;
using ServiceCommunication.Core.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace ServiceCommunication.Core.ApplicationService.Interfaces
{
    public interface IUserService
    {
        Task<User> CreateUser(NewUser user);
        Task<User> FindUserById(int id);
        Task<ICollection<User>> GetAllUsers();
        Task<User> UpdateUser(int Id, NewUser userUpdate);
        Task DeleteUser(int id);
        Task<User> AddUserToTeam(int userId, int teamId);
    }
}
