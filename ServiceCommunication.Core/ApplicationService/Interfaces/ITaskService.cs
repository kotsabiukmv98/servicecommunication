﻿using ServiceCommunication.Core.ApplicationService.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ServiceCommunication.Core.ApplicationService.Interfaces
{
    public interface ITaskService
    {
        Task<Entity.Task> CreateTask(NewTask task);
        Task<Entity.Task> FindTaskById(int id);
        Task<ICollection<Entity.Task>> GetAllTasks();
        Task<Entity.Task> UpdateTask(int id, NewTask taskUpdate);
        Task DeleteTask(int id);
        Task MarkTaskAsFinished(int id);
    }
}
