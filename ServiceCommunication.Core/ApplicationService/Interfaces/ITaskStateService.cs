﻿using ServiceCommunication.Core.ApplicationService.Models;
using ServiceCommunication.Core.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace ServiceCommunication.Core.ApplicationService.Interfaces
{
    public interface ITaskStateService
    {
        Task<TaskState> CreateTaskState(NewTaskState taskState);
        Task<TaskState> FindTaskStateById(int id);
        Task<ICollection<TaskState>> GetAllTaskStates();
        Task<TaskState> UpdateTaskState(int id, NewTaskState taskStateUpdate);
        Task DeleteTaskState(int id);
    }
}
