﻿using ServiceCommunication.Core.ApplicationService.Models;
using ServiceCommunication.Core.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace ServiceCommunication.Core.ApplicationService.Interfaces
{
    public interface IProjectService
    {
        Task<Project> CreateProject(NewProject project);
        Task<Project> FindProjectById(int id);
        Task<ICollection<Project>> GetAllProjects();
        Task<Project> UpdateProject(int id, NewProject projectUpdate);
        Task DeleteProject(int id);
    }
}