﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace ServiceCommunication.Core.DomainService
{
    public interface ITaskRepository
    {
        Task<Entity.Task> Create(Entity.Task task);
        Task<Entity.Task> ReadById(int id);
        Task<ICollection<Entity.Task>> ReadAll();
        Task<Entity.Task> Update(Entity.Task taskUpdate);
        Task Delete(int id);
    }
}
