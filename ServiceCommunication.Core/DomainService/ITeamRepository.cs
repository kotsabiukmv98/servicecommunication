﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ServiceCommunication.Core.Entity;
using Task = System.Threading.Tasks.Task;

namespace ServiceCommunication.Core.DomainService
{
    public interface ITeamRepository
    {
        Task<Team> Create(Team team);
        Task<Team> ReadById(int id);
        Task<ICollection<Team>> ReadAll();
        Task<Team> Update(Team teamUpdate);
        Task Delete(int id);
    }
}
