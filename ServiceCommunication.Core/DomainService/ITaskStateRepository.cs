﻿using ServiceCommunication.Core.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace ServiceCommunication.Core.DomainService
{
    public interface ITaskStateRepository
    {
        Task<TaskState> Create(TaskState taskState);
        Task<TaskState> ReadById(int id);
        Task<ICollection<TaskState>> ReadAll();
        Task<TaskState> Update(TaskState taskStateUpdate);
        Task Delete(int id);
    }
}
