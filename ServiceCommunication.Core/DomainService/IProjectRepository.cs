﻿using ServiceCommunication.Core.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace ServiceCommunication.Core.DomainService
{
    public interface IProjectRepository
    {
        Task<Project> Create(Project project);
        Task<Project> ReadById(int id);
        Task<ICollection<Project>> ReadAll();
        Task<Project> Update(Project projectUpdate);
        Task Delete(int id);
    }
}
