﻿using ServiceCommunication.Core.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace ServiceCommunication.Core.DomainService
{
    public interface IUserRepository
    {
        Task<User> Create(User customer);
        Task<User> ReadById(int id);
        Task<ICollection<User>> ReadAll();
        Task<User> Update(User userUpdate);
        Task Delete(int id);
    }
}
