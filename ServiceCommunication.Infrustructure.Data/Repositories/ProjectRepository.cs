﻿using Microsoft.EntityFrameworkCore;
using ServiceCommunication.Core.DomainService;
using ServiceCommunication.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace ServiceCommunication.Infrustructure.Data
{
    public class ProjectRepository : IProjectRepository
    {
        private ApplicationDbContext _context;

        public ProjectRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Project> Create(Project project)
        {
            project.CreatedAt = DateTime.Now;

            _context.Projects.Add(project);
            await _context.SaveChangesAsync();
            return project;
        }

        public async Task Delete(int id)
        {
            var project = await ReadById(id);
            if (project == null)
                return;

            _context.Projects.Remove(project);
            await _context.SaveChangesAsync();
        }

        public async Task<ICollection<Project>> ReadAll()
        {
            return await _context.Projects.ToListAsync();
        }

        public async Task<Project> ReadById(int id)
        {
            return await _context.Projects.FindAsync(id);
        }

        public async Task<Project> Update(Project projectUpdate)
        {
            var project = await ReadById(projectUpdate.Id);
            if (project == null)
                return null;

            project.Name = projectUpdate.Name;
            project.Description = projectUpdate.Description;
            project.AuthorId = projectUpdate.AuthorId;
            project.TeamId = projectUpdate.TeamId;
            project.Deadline = projectUpdate.Deadline;

            _context.Projects.Update(project);
            await _context.SaveChangesAsync();
            return project;
        }
    }
}
