﻿using Microsoft.EntityFrameworkCore;
using ServiceCommunication.Core.DomainService;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace ServiceCommunication.Infrustructure.Data
{
    public class TaskRepository : ITaskRepository
    {
        private ApplicationDbContext _context;

        public TaskRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<Core.Entity.Task> Create(Core.Entity.Task task)
        {
            task.CreatedAt = DateTime.Now;

            _context.Tasks.Add(task);
            await _context.SaveChangesAsync();
            return task;
        }

        public async Task Delete(int id)
        {
            var task = await ReadById(id);
            if (task == null)
                return;

            _context.Tasks.Remove(task);
            await _context.SaveChangesAsync();
        }

        public async Task<ICollection<Core.Entity.Task>> ReadAll()
        {
            return await _context.Tasks.ToListAsync();
        }

        public async Task<Core.Entity.Task> ReadById(int id)
        {
            return await _context.Tasks.FindAsync(id);
        }

        public async Task<Core.Entity.Task> Update(Core.Entity.Task taskUpdate)
        {
            var task =  await ReadById(taskUpdate.Id);
            if (task == null)
                return null;

            task.Name = taskUpdate.Name;
            task.Description = taskUpdate.Description;
            task.PerformerId = taskUpdate.PerformerId;
            task.ProjectId = taskUpdate.ProjectId;
            task.TaskStateId = taskUpdate.TaskStateId;

            _context.Tasks.Update(task);
            await _context.SaveChangesAsync();
            return task;
        }
    }
}
