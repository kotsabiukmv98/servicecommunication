﻿using Microsoft.EntityFrameworkCore;
using ServiceCommunication.Core.DomainService;
using ServiceCommunication.Core.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace ServiceCommunication.Infrustructure.Data
{
    public class TaskStateRepository : ITaskStateRepository
    {
        private ApplicationDbContext _context;

        public TaskStateRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<TaskState> Create(TaskState taskState)
        {
            _context.TaskStates.Add(taskState);
            await _context.SaveChangesAsync();
            return taskState;
        }

        public async Task Delete(int id)
        {
            var taskState = await ReadById(id);
            if (taskState == null)
                return;

            _context.TaskStates.Remove(taskState);
            await _context.SaveChangesAsync();
        }

        public async Task<ICollection<TaskState>> ReadAll()
        {
            return await _context.TaskStates.ToListAsync();
        }

        public async Task<TaskState> ReadById(int id)
        {
            return await _context.TaskStates.FindAsync(id);
        }

        public async Task<TaskState> Update(TaskState taskStateUpdate)
        {
            var taskState = await ReadById(taskStateUpdate.Id);
            if (taskState == null)
                return null;

            taskState.Value = taskStateUpdate.Value;
            _context.TaskStates.Update(taskState);
            await _context.SaveChangesAsync();
            return taskState;
        }
    }
}
