﻿using Microsoft.EntityFrameworkCore;
using ServiceCommunication.Core.DomainService;
using ServiceCommunication.Core.Entity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace ServiceCommunication.Infrustructure.Data
{
    public class TeamRepository : ITeamRepository
    {
        private ApplicationDbContext _context;

        public TeamRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Team> Create(Team team)
        {
            team.CreatedAt = DateTime.Now;

            _context.Teams.Add(team);
            await _context.SaveChangesAsync();
            return team;
        }

        public async Task Delete(int id)
        {
            var team = await ReadById(id);
            if (team == null)
                return;

            _context.Teams.Remove(team);
            await _context.SaveChangesAsync();
        }

        public async Task<ICollection<Team>> ReadAll()
        {
            return await _context.Teams.ToListAsync();
        }

        public async Task<Team> ReadById(int id)
        {
            return await _context.Teams.FindAsync(id);
        }

        public async Task<Team> Update(Team teamUpdate)
        {
            var team = await ReadById(teamUpdate.Id);
            if (team == null)
                return null;
            team.Name = teamUpdate.Name;
            _context.Teams.Update(team);
            await _context.SaveChangesAsync();

            return team;
        }
    }
}
