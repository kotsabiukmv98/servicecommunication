﻿using Microsoft.EntityFrameworkCore;
using ServiceCommunication.Core.DomainService;
using ServiceCommunication.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace ServiceCommunication.Infrustructure.Data
{
    public class UserRepository : IUserRepository
    {
        private ApplicationDbContext _context;

        public UserRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<User> Create(User user)
        {
            user.RegisteredAt = DateTime.Now;
            
            _context.Users.Add(user);
            await _context.SaveChangesAsync();
            return user;
        }

        public async Task Delete(int id)
        {
            var user = await ReadById(id);
            if (user == null)
                return;

            _context.Users.Remove(user);
            await _context.SaveChangesAsync();
        }

        public async Task<ICollection<User>> ReadAll()
        {
            return await _context.Users.ToListAsync();
        }

        public async Task<User> ReadById(int id)
        {
            return await _context.Users.FindAsync(id);
        }

        public async Task<User> Update(User userUpdate)
        {
            var user = await ReadById(userUpdate.Id);
            if (user == null)
                return null;

            user.FirstName = userUpdate.FirstName;
            user.LastName = userUpdate.LastName;
            user.Email = userUpdate.Email;
            user.TeamId = userUpdate.TeamId;

            _context.Users.Update(user);
            await _context.SaveChangesAsync();
            return user;
        }
    }
}
