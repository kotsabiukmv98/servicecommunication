﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ServiceCommunication.Infrustructure.Data.Migrations
{
    public partial class AddUsersWithNullTeamId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Birthday", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[,]
                {
                    { 10, new DateTime(2015, 9, 5, 1, 5, 16, 586, DateTimeKind.Local).AddTicks(3480), "Felix53@hotmail.com", "Felix", "Upton", new DateTime(2019, 5, 26, 14, 37, 10, 213, DateTimeKind.Local).AddTicks(2589), null },
                    { 15, new DateTime(2005, 10, 7, 23, 8, 42, 685, DateTimeKind.Local).AddTicks(7458), "Ismael.Lowe20@yahoo.com", "Ismael", "Lowe", new DateTime(2019, 6, 15, 8, 21, 18, 315, DateTimeKind.Local).AddTicks(3026), null },
                    { 18, new DateTime(2006, 5, 17, 19, 51, 1, 873, DateTimeKind.Local).AddTicks(5246), "Nelle_Gislason@gmail.com", "Nelle", "Gislason", new DateTime(2019, 6, 19, 9, 32, 41, 408, DateTimeKind.Local).AddTicks(2010), null },
                    { 19, new DateTime(2014, 7, 28, 6, 49, 22, 796, DateTimeKind.Local).AddTicks(976), "Ellsworth27@hotmail.com", "Ellsworth", "Gusikowski", new DateTime(2019, 5, 13, 1, 21, 1, 273, DateTimeKind.Local).AddTicks(2632), null },
                    { 21, new DateTime(2009, 12, 27, 15, 48, 8, 640, DateTimeKind.Local).AddTicks(7910), "Zane.Torphy@hotmail.com", "Zane", "Torphy", new DateTime(2019, 5, 9, 22, 50, 49, 994, DateTimeKind.Local).AddTicks(5877), null },
                    { 26, new DateTime(2014, 5, 5, 5, 17, 12, 748, DateTimeKind.Local).AddTicks(2342), "Ryley27@hotmail.com", "Ryley", "Dooley", new DateTime(2019, 6, 13, 5, 49, 38, 349, DateTimeKind.Local).AddTicks(7166), null },
                    { 29, new DateTime(2008, 2, 18, 23, 23, 15, 362, DateTimeKind.Local).AddTicks(6702), "Nicole.Harris@gmail.com", "Nicole", "Harris", new DateTime(2019, 5, 19, 20, 16, 48, 939, DateTimeKind.Local).AddTicks(3096), null },
                    { 31, new DateTime(2013, 10, 17, 17, 39, 36, 219, DateTimeKind.Local).AddTicks(1412), "Niko40@gmail.com", "Niko", "Jaskolski", new DateTime(2019, 5, 16, 20, 24, 16, 986, DateTimeKind.Local).AddTicks(356), null },
                    { 36, new DateTime(2018, 9, 11, 22, 3, 48, 471, DateTimeKind.Local).AddTicks(6157), "Jamil.Dare43@hotmail.com", "Jamil", "Dare", new DateTime(2019, 5, 24, 0, 49, 15, 774, DateTimeKind.Local).AddTicks(4475), null },
                    { 39, new DateTime(2018, 2, 1, 11, 52, 42, 565, DateTimeKind.Local).AddTicks(6243), "June_Lind63@hotmail.com", "June", "Lind", new DateTime(2019, 5, 15, 14, 20, 9, 179, DateTimeKind.Local).AddTicks(5395), null },
                    { 44, new DateTime(2010, 9, 29, 10, 30, 0, 52, DateTimeKind.Local).AddTicks(7377), "Jameson68@hotmail.com", "Jameson", "Sipes", new DateTime(2019, 6, 18, 18, 56, 54, 494, DateTimeKind.Local).AddTicks(1041), null },
                    { 49, new DateTime(2015, 1, 28, 14, 24, 10, 413, DateTimeKind.Local).AddTicks(1832), "Flavie_Ryan77@yahoo.com", "Flavie", "Ryan", new DateTime(2019, 5, 30, 10, 46, 31, 909, DateTimeKind.Local).AddTicks(8133), null },
                    { 50, new DateTime(2004, 10, 1, 6, 48, 20, 115, DateTimeKind.Local).AddTicks(3595), "Andreanne64@gmail.com", "Andreanne", "Swift", new DateTime(2019, 6, 12, 19, 47, 30, 0, DateTimeKind.Local).AddTicks(7381), null }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 26);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 29);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 31);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 36);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 39);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 44);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 49);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 50);
        }
    }
}
