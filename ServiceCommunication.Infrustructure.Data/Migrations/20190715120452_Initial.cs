﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ServiceCommunication.Infrustructure.Data.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TaskStates",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Value = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaskStates", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Teams",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Teams", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FirstName = table.Column<string>(nullable: false),
                    LastName = table.Column<string>(nullable: false),
                    Email = table.Column<string>(nullable: true),
                    RegisteredAt = table.Column<DateTime>(nullable: false),
                    TeamId = table.Column<int>(nullable: true),
                    Birthday = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_Teams_TeamId",
                        column: x => x.TeamId,
                        principalTable: "Teams",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "TaskStates",
                columns: new[] { "Id", "Value" },
                values: new object[,]
                {
                    { 1, "Created" },
                    { 2, "Started" },
                    { 3, "Finished" },
                    { 4, "Canceled" }
                });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[,]
                {
                    { 2, new DateTime(2019, 6, 19, 4, 0, 24, 867, DateTimeKind.Local).AddTicks(4205), "labore" },
                    { 3, new DateTime(2019, 6, 19, 9, 38, 3, 576, DateTimeKind.Local).AddTicks(8509), "sunt" },
                    { 4, new DateTime(2019, 6, 19, 7, 38, 28, 291, DateTimeKind.Local).AddTicks(8990), "reprehenderit" },
                    { 5, new DateTime(2019, 6, 18, 16, 41, 51, 862, DateTimeKind.Local).AddTicks(1697), "qui" },
                    { 6, new DateTime(2019, 6, 18, 14, 59, 49, 945, DateTimeKind.Local).AddTicks(7501), "nostrum" },
                    { 7, new DateTime(2019, 6, 19, 7, 4, 32, 306, DateTimeKind.Local).AddTicks(960), "quo" },
                    { 8, new DateTime(2019, 6, 19, 1, 24, 41, 788, DateTimeKind.Local).AddTicks(2461), "repudiandae" },
                    { 9, new DateTime(2019, 6, 18, 20, 1, 8, 690, DateTimeKind.Local).AddTicks(6840), "inventore" },
                    { 10, new DateTime(2019, 6, 19, 4, 6, 16, 949, DateTimeKind.Local).AddTicks(6355), "nobis" },
                    { 11, new DateTime(2019, 6, 18, 20, 3, 21, 284, DateTimeKind.Local).AddTicks(9057), "reprehenderit" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Birthday", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[,]
                {
                    { 9, new DateTime(2010, 2, 5, 11, 36, 37, 60, DateTimeKind.Local).AddTicks(8560), "Claudie_Ward68@yahoo.com", "Claudie", "Ward", new DateTime(2019, 5, 29, 9, 32, 7, 35, DateTimeKind.Local).AddTicks(1228), 2 },
                    { 34, new DateTime(2006, 7, 3, 16, 41, 31, 135, DateTimeKind.Local).AddTicks(3373), "Reagan_Ortiz@yahoo.com", "Reagan", "Ortiz", new DateTime(2019, 5, 9, 17, 30, 24, 249, DateTimeKind.Local).AddTicks(7100), 6 },
                    { 35, new DateTime(2016, 11, 10, 18, 25, 57, 408, DateTimeKind.Local).AddTicks(2556), "Obie80@yahoo.com", "Obie", "Gutkowski", new DateTime(2019, 5, 19, 14, 40, 33, 783, DateTimeKind.Local).AddTicks(1679), 6 },
                    { 38, new DateTime(2008, 1, 16, 4, 32, 50, 313, DateTimeKind.Local).AddTicks(534), "Kurt65@gmail.com", "Kurt", "Oberbrunner", new DateTime(2019, 6, 11, 7, 58, 44, 480, DateTimeKind.Local).AddTicks(1038), 6 },
                    { 45, new DateTime(2003, 3, 8, 20, 42, 37, 357, DateTimeKind.Local).AddTicks(1870), "Verda16@hotmail.com", "Verda", "Kerluke", new DateTime(2019, 5, 31, 21, 23, 18, 772, DateTimeKind.Local).AddTicks(1258), 6 },
                    { 48, new DateTime(2004, 5, 30, 15, 11, 31, 543, DateTimeKind.Local).AddTicks(2933), "Lorenzo25@hotmail.com", "Lorenzo", "Connelly", new DateTime(2019, 5, 25, 4, 4, 24, 603, DateTimeKind.Local).AddTicks(755), 6 },
                    { 6, new DateTime(2001, 10, 3, 3, 56, 53, 85, DateTimeKind.Local).AddTicks(8521), "Frederik.Braun@hotmail.com", "Frederik", "Braun", new DateTime(2019, 5, 13, 23, 39, 51, 581, DateTimeKind.Local).AddTicks(8085), 8 },
                    { 14, new DateTime(2008, 5, 6, 8, 49, 53, 842, DateTimeKind.Local).AddTicks(1262), "Wilbert95@hotmail.com", "Wilbert", "Hintz", new DateTime(2019, 5, 11, 7, 29, 49, 538, DateTimeKind.Local).AddTicks(330), 8 },
                    { 16, new DateTime(2018, 5, 14, 18, 45, 58, 783, DateTimeKind.Local).AddTicks(9728), "Vella_Daniel@hotmail.com", "Vella", "Daniel", new DateTime(2019, 6, 7, 12, 4, 33, 127, DateTimeKind.Local).AddTicks(4), 8 },
                    { 30, new DateTime(2006, 12, 28, 9, 4, 20, 778, DateTimeKind.Local).AddTicks(2572), "Shannon.Hayes@yahoo.com", "Shannon", "Hayes", new DateTime(2019, 5, 10, 1, 3, 29, 824, DateTimeKind.Local).AddTicks(3494), 8 },
                    { 46, new DateTime(2008, 5, 25, 20, 26, 25, 493, DateTimeKind.Local).AddTicks(8895), "Miguel28@hotmail.com", "Miguel", "Ondricka", new DateTime(2019, 5, 14, 21, 44, 13, 911, DateTimeKind.Local).AddTicks(2226), 8 },
                    { 8, new DateTime(2010, 2, 11, 13, 18, 41, 654, DateTimeKind.Local).AddTicks(7792), "Jeanette.Shields@yahoo.com", "Jeanette", "Shields", new DateTime(2019, 5, 17, 10, 48, 49, 516, DateTimeKind.Local).AddTicks(1184), 9 },
                    { 17, new DateTime(2010, 4, 19, 20, 53, 23, 251, DateTimeKind.Local).AddTicks(8429), "Anibal_Rice27@gmail.com", "Anibal", "Rice", new DateTime(2019, 6, 14, 4, 0, 10, 670, DateTimeKind.Local).AddTicks(1), 9 },
                    { 43, new DateTime(2001, 10, 22, 0, 31, 18, 172, DateTimeKind.Local).AddTicks(2308), "Caitlyn_Upton@yahoo.com", "Caitlyn", "Upton", new DateTime(2019, 6, 15, 10, 35, 47, 469, DateTimeKind.Local).AddTicks(8331), 9 },
                    { 7, new DateTime(2014, 6, 1, 16, 47, 4, 401, DateTimeKind.Local).AddTicks(5291), "Geovanny.Grimes@gmail.com", "Geovanny", "Grimes", new DateTime(2019, 5, 24, 19, 4, 46, 542, DateTimeKind.Local).AddTicks(8431), 11 },
                    { 24, new DateTime(2012, 10, 25, 23, 28, 59, 884, DateTimeKind.Local).AddTicks(72), "Virginia.VonRueden83@yahoo.com", "Virginia", "VonRueden", new DateTime(2019, 5, 14, 6, 6, 50, 465, DateTimeKind.Local).AddTicks(5118), 11 },
                    { 23, new DateTime(2005, 3, 14, 7, 56, 43, 445, DateTimeKind.Local).AddTicks(62), "Dewitt_Macejkovic19@gmail.com", "Dewitt", "Macejkovic", new DateTime(2019, 5, 30, 18, 34, 19, 597, DateTimeKind.Local).AddTicks(5857), 6 },
                    { 37, new DateTime(2011, 11, 10, 19, 36, 25, 464, DateTimeKind.Local).AddTicks(6624), "Tara_Hessel20@gmail.com", "Tara", "Hessel", new DateTime(2019, 5, 20, 17, 34, 25, 355, DateTimeKind.Local).AddTicks(5573), 11 },
                    { 5, new DateTime(2006, 8, 23, 15, 25, 3, 664, DateTimeKind.Local).AddTicks(2653), "Teagan17@gmail.com", "Teagan", "Collier", new DateTime(2019, 6, 9, 21, 25, 30, 120, DateTimeKind.Local).AddTicks(2628), 6 },
                    { 20, new DateTime(2012, 8, 18, 8, 30, 43, 486, DateTimeKind.Local).AddTicks(66), "Abel29@yahoo.com", "Abel", "Robel", new DateTime(2019, 6, 5, 18, 1, 22, 296, DateTimeKind.Local).AddTicks(96), 5 },
                    { 12, new DateTime(2013, 4, 14, 23, 10, 15, 731, DateTimeKind.Local).AddTicks(2594), "Murray_Hegmann84@gmail.com", "Murray", "Hegmann", new DateTime(2019, 5, 30, 18, 37, 26, 603, DateTimeKind.Local).AddTicks(2667), 2 },
                    { 25, new DateTime(2005, 6, 16, 10, 0, 13, 325, DateTimeKind.Local).AddTicks(4049), "Gideon92@hotmail.com", "Gideon", "Treutel", new DateTime(2019, 5, 12, 21, 49, 31, 718, DateTimeKind.Local).AddTicks(5296), 2 },
                    { 27, new DateTime(2005, 2, 27, 19, 29, 33, 877, DateTimeKind.Local).AddTicks(7103), "Alessandra_Rippin@gmail.com", "Alessandra", "Rippin", new DateTime(2019, 6, 2, 1, 2, 15, 366, DateTimeKind.Local).AddTicks(846), 2 },
                    { 28, new DateTime(2015, 3, 3, 23, 21, 40, 895, DateTimeKind.Local).AddTicks(5746), "Salvador.Brekke79@yahoo.com", "Salvador", "Brekke", new DateTime(2019, 5, 27, 15, 51, 30, 457, DateTimeKind.Local).AddTicks(5344), 2 },
                    { 32, new DateTime(2004, 5, 13, 7, 38, 25, 616, DateTimeKind.Local).AddTicks(5587), "Herta29@hotmail.com", "Herta", "Hermann", new DateTime(2019, 5, 10, 6, 30, 39, 727, DateTimeKind.Local).AddTicks(886), 2 },
                    { 42, new DateTime(2010, 1, 25, 6, 5, 25, 945, DateTimeKind.Local).AddTicks(7497), "Athena.Macejkovic@hotmail.com", "Athena", "Macejkovic", new DateTime(2019, 6, 2, 13, 6, 42, 817, DateTimeKind.Local).AddTicks(2448), 2 },
                    { 47, new DateTime(2008, 10, 27, 12, 10, 36, 276, DateTimeKind.Local).AddTicks(490), "Dayna.Toy@gmail.com", "Dayna", "Toy", new DateTime(2019, 5, 13, 16, 48, 29, 871, DateTimeKind.Local).AddTicks(8551), 2 },
                    { 2, new DateTime(2013, 11, 7, 11, 10, 32, 210, DateTimeKind.Local).AddTicks(6017), "Gayle_Swift98@yahoo.com", "Gayle", "Swift", new DateTime(2019, 6, 17, 16, 1, 47, 495, DateTimeKind.Local).AddTicks(1777), 3 },
                    { 3, new DateTime(2017, 10, 11, 15, 23, 43, 460, DateTimeKind.Local).AddTicks(9496), "Tad_Powlowski27@hotmail.com", "Tad", "Powlowski", new DateTime(2019, 6, 11, 5, 3, 33, 262, DateTimeKind.Local).AddTicks(1339), 3 },
                    { 11, new DateTime(2008, 1, 12, 19, 14, 0, 183, DateTimeKind.Local).AddTicks(7284), "Alessandra_Witting72@gmail.com", "Alessandra", "Witting", new DateTime(2019, 5, 22, 16, 24, 28, 921, DateTimeKind.Local).AddTicks(3083), 3 },
                    { 13, new DateTime(2005, 7, 6, 0, 26, 42, 754, DateTimeKind.Local).AddTicks(3155), "Jaron69@hotmail.com", "Jaron", "McKenzie", new DateTime(2019, 5, 14, 11, 2, 18, 296, DateTimeKind.Local).AddTicks(2720), 3 },
                    { 22, new DateTime(2006, 7, 9, 2, 16, 46, 940, DateTimeKind.Local).AddTicks(7364), "Danyka_Bernier@yahoo.com", "Danyka", "Bernier", new DateTime(2019, 5, 31, 18, 59, 6, 419, DateTimeKind.Local).AddTicks(8805), 3 },
                    { 33, new DateTime(2001, 9, 30, 9, 49, 45, 587, DateTimeKind.Local).AddTicks(9192), "Mozell.Sauer54@yahoo.com", "Mozell", "Sauer", new DateTime(2019, 5, 26, 14, 47, 24, 566, DateTimeKind.Local).AddTicks(1398), 3 },
                    { 40, new DateTime(2008, 3, 11, 0, 29, 4, 643, DateTimeKind.Local).AddTicks(5853), "Gage38@gmail.com", "Gage", "Grimes", new DateTime(2019, 5, 14, 3, 50, 38, 788, DateTimeKind.Local).AddTicks(6702), 3 },
                    { 4, new DateTime(2002, 5, 11, 18, 44, 15, 899, DateTimeKind.Local).AddTicks(5244), "Letha.Strosin58@gmail.com", "Letha", "Strosin", new DateTime(2019, 6, 17, 5, 56, 24, 847, DateTimeKind.Local).AddTicks(1274), 5 },
                    { 51, new DateTime(2003, 4, 20, 5, 16, 37, 735, DateTimeKind.Local).AddTicks(8838), "Wyatt_Torphy@hotmail.com", "Wyatt", "Torphy", new DateTime(2019, 6, 3, 13, 14, 39, 627, DateTimeKind.Local).AddTicks(5642), 5 },
                    { 41, new DateTime(2017, 10, 22, 9, 8, 16, 144, DateTimeKind.Local).AddTicks(621), "Ryleigh10@gmail.com", "Ryleigh", "Herzog", new DateTime(2019, 6, 13, 2, 18, 40, 395, DateTimeKind.Local).AddTicks(9234), 11 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Users_TeamId",
                table: "Users",
                column: "TeamId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TaskStates");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Teams");
        }
    }
}
