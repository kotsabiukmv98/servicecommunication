﻿using Microsoft.EntityFrameworkCore;
using ServiceCommunication.Core.Entity;

namespace ServiceCommunication.Infrustructure.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Project> Projects { get; private set; }
        public DbSet<Task> Tasks { get; private set; }
        public DbSet<TaskState> TaskStates { get; private set; }
        public DbSet<Team> Teams { get; private set; }
        public DbSet<User> Users { get; private set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Configure();
            modelBuilder.Seed();

            base.OnModelCreating(modelBuilder);
        }
    }
}
