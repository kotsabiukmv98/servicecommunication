﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using ServiceCommunication.Core.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace ServiceCommunication.Infrustructure.Data
{
    static class ModelBuilderExtensions
    {
        public static void Configure(this ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<TaskState>().Property(ts => ts.Value).IsRequired();

            modelBuilder.Entity<Task>().Property(t => t.Name).IsRequired();
            modelBuilder.Entity<Task>().Property(t => t.Description).IsRequired();
            modelBuilder.Entity<Task>()
                .HasOne(t => t.TaskState)
                .WithMany(ts => ts.Tasks)
                .HasForeignKey(t => t.TaskStateId)
                .OnDelete(DeleteBehavior.SetNull);
            modelBuilder.Entity<Task>()
                .HasOne(t => t.Performer)
                .WithMany(u => u.Tasks)
                .HasForeignKey(t => t.PerformerId)
                .OnDelete(DeleteBehavior.SetNull);
            modelBuilder.Entity<Task>()
                .HasOne(t => t.Project)
                .WithMany(p => p.Tasks)
                .HasForeignKey(t => t.ProjectId)
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Project>().Property(p => p.Name).IsRequired();
            modelBuilder.Entity<Project>().Property(p => p.Description).IsRequired();
            modelBuilder.Entity<Project>()
                .HasOne(p => p.Author)
                .WithMany(u => u.CreatedProjects)
                .HasForeignKey(p => p.AuthorId)
                .OnDelete(DeleteBehavior.SetNull);
            modelBuilder.Entity<Project>()
                .HasOne(p => p.Team)
                .WithMany(t => t.Projects)
                .HasForeignKey(p => p.TeamId)
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Team>().Property(t => t.Name).IsRequired();

            modelBuilder.Entity<User>().Property(t => t.FirstName).IsRequired();
            modelBuilder.Entity<User>().Property(t => t.LastName).IsRequired();
            modelBuilder.Entity<User>()
                .HasOne(u => u.Team)
                .WithMany(t => t.Teammates)
                .HasForeignKey(u => u.TeamId)
                .OnDelete(DeleteBehavior.SetNull);
        }
        public static void Seed(this ModelBuilder modelBuilder)
        {
            //var currDir = Directory.GetCurrentDirectory() + "./bin/Debug/netcoreapp2.2/Data/";
            var currDir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var projects = JsonConvert.DeserializeObject<List<Project>>(File.ReadAllText(currDir + "/Data/projects.json"))
                .Select(p => { p.Id++; p.AuthorId++; p.TeamId++; return p; });
            var tasks = JsonConvert.DeserializeObject<List<Task>>(File.ReadAllText(currDir + "/Data/tasks.json"))
                .Select(t => { t.Id++; t.PerformerId++; t.ProjectId++; t.TaskStateId++; return t; });
            var taskStates = JsonConvert.DeserializeObject<List<TaskState>>(File.ReadAllText(currDir + "/Data/taskstates.json"))
                .Select(ts => { ts.Id++; return ts; });
            var teams = JsonConvert.DeserializeObject<List<Team>>(File.ReadAllText(currDir + "/Data/teams.json"))
                .Select(t => { t.Id++; return t; });
            var users = JsonConvert.DeserializeObject<List<User>>(File.ReadAllText(currDir + "/Data/users.json"))
                .Select(u => { u.Id++; u.TeamId++; return u; });

            modelBuilder.Entity<TaskState>().HasData(taskStates);
            modelBuilder.Entity<Team>().HasData(teams);
            modelBuilder.Entity<User>().HasData(users);
            modelBuilder.Entity<Project>().HasData(projects);
            modelBuilder.Entity<Task>().HasData(tasks);
        }
    }
}
