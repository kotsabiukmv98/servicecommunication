﻿using AutoMapper;
using ServiceCommunication.Core.ApplicationService.Models;
using ServiceCommunication.Core.Entity;
using Task = ServiceCommunication.Core.Entity.Task;

namespace ServiceCommunication.API
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<NewTeam, Team>();
            CreateMap<NewUser, User>();
            CreateMap<NewProject, Project>();
            CreateMap<NewTask, Task>();
            CreateMap<NewTaskState, TaskState>();
        }
    }
}
