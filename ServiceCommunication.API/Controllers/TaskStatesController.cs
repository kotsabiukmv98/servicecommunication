﻿using Microsoft.AspNetCore.Mvc;
using ServiceCommunication.Core.ApplicationService.Interfaces;
using ServiceCommunication.Core.ApplicationService.Models;
using ServiceCommunication.Core.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace ServiceCommunication.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskStatesController : ControllerBase
    {
        private readonly ITaskStateService _taskStateService;

        public TaskStatesController(ITaskStateService taskStateService)
        {
            _taskStateService = taskStateService;
        }

        // GET: api/TaskStates
        [HttpGet]
        public async Task<ActionResult<ICollection<TaskState>>> Get()
        {
            return Ok(await _taskStateService.GetAllTaskStates());
        }

        // GET: api/TaskStates/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TaskState>> Get(int id)
        {
            var taskState = await _taskStateService.FindTaskStateById(id);
            if (taskState == null)
            {
                return StatusCode(404, $"Did not find task state with ID {id}");
            }
            return Ok(taskState);
        }

        // POST: api/TaskStates
        [HttpPost]
        public async Task<ActionResult<TaskState>> Post([FromBody] NewTaskState newTaskState)
        {
            if (string.IsNullOrEmpty(newTaskState.Value))
            {
                return BadRequest("Value is required for creating TaskStatus");
            }

            return Ok(await _taskStateService.CreateTaskState(newTaskState));
        }

        // PUT: api/TaskStates/5
        [HttpPut("{id}")]
        public async Task<ActionResult<TaskState>> Put(int id, [FromBody] NewTaskState newTaskState)
        {
            var taskState = await _taskStateService.UpdateTaskState(id, newTaskState);
            if (taskState == null)
            {
                return StatusCode(404, $"Did not find task state with ID {id}");
            }

            return Ok(taskState);
        }

        // DELETE: api/TaskStates/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await _taskStateService.DeleteTaskState(id);
            return NoContent();
        }
    }
}
