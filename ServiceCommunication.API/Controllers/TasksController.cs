﻿using Microsoft.AspNetCore.Mvc;
using ServiceCommunication.Core.ApplicationService.Interfaces;
using ServiceCommunication.Core.ApplicationService.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using Task = ServiceCommunication.Core.Entity.Task;

namespace ServiceCommunication.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly ITaskService _taskService;

        public TasksController(ITaskService taskService)
        {
            _taskService = taskService;
        }

        // GET: api/Tasks
        [HttpGet]
        public async Task<ActionResult<ICollection<Task>>> Get()
        {
            return Ok(await _taskService.GetAllTasks());
        }

        // GET: api/Tasks/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Task>> Get(int id)
        {
            var task = await _taskService.FindTaskById(id);
            if (task == null)
            {
                return StatusCode(404, $"Did not find task with ID {id}");
            }
            return Ok(task);
        }

        // POST: api/Tasks
        [HttpPost]
        public async Task<ActionResult<Task>> Post([FromBody] NewTask newTask)
        {
            if (string.IsNullOrEmpty(newTask.Name))
            {
                return BadRequest("Name is required for creating Task");
            }

            return Ok(await _taskService.CreateTask(newTask));
        }

        // PUT: api/Tasks/5
        [HttpPut("{id}")]
        public async Task<ActionResult<Task>> Put(int id, [FromBody] NewTask newTask)
        {
            var task = await _taskService.UpdateTask(id, newTask);
            if (task == null)
            {
                return StatusCode(404, $"Did not find task with ID {id}");
            }

            return Ok(task);
        }

        // DELETE: api/Tasks/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await _taskService.DeleteTask(id);
            return NoContent();
        }

        // DELETE: api/Tasks/5
        [HttpGet("marktaskasfinished/{id}")]

        public async Task<ActionResult> MarkTaskAsFinished(int id)
        {
            await _taskService.MarkTaskAsFinished(id);
            return NoContent();
        }


    }
}
