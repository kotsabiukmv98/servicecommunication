﻿using Microsoft.AspNetCore.Mvc;
using ServiceCommunication.Core.ApplicationService.Interfaces;
using ServiceCommunication.Core.ApplicationService.Models;
using ServiceCommunication.Core.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ServiceCommunication.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly ITeamService _teamService;

        public TeamsController(ITeamService teamService)
        {
            _teamService = teamService;
        }

        // GET: api/Teams
        [HttpGet]
        public async Task<ActionResult<ICollection<Team>>> Get()
        {
            return Ok(await _teamService.GetAllTeams());
        }

        // GET: api/Teams/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Team>> Get(int id)
        {
            var team = await _teamService.FindTeamById(id);
            if (team == null)
            {
                return StatusCode(404, $"Did not find team with ID {id}");
            }
            return Ok(team);
        }

        // POST: api/Teams
        [HttpPost]
        public async Task<ActionResult<Team>> Post([FromBody] NewTeam newTeam)
        {
            if (string.IsNullOrEmpty(newTeam.Name))
            {
                return BadRequest("Name is required for creating Team");
            }
            return Ok(await _teamService.CreateTeam(newTeam));
        }

        // PUT: api/Teams/5
        [HttpPut("{id}")]
        public async Task<ActionResult<Team>> Put(int id, [FromBody] NewTeam newTeam)
        {
            var team = await _teamService.UpdateTeam(id, newTeam);
            if (team == null)
            {
                return StatusCode(404, $"Did not find team with ID {id}");
            }

            return Ok(team);
        }

        // DELETE: api/Teams/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await  _teamService.DeleteTeam(id);
            return NoContent();
        }
    }
}
