﻿using Microsoft.AspNetCore.Mvc;
using ServiceCommunication.Core.ApplicationService.Interfaces;
using ServiceCommunication.Core.ApplicationService.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ServiceCommunication.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QueryController : ControllerBase
    {
        private readonly IQueryService _queryService;

        public QueryController(IQueryService queryService)
        {
            _queryService = queryService;
        }

        [HttpGet("GetAmountOfUserTasksGroupedByProjects/{id}")]
        public async Task<ActionResult<List<GroupedTasks>>> GetAmountOfUserTasksGroupedByProjects(int id)
        {
            return Ok(await _queryService.GetAmountOfUserTasksGroupedByProjects(id));
        }

        [HttpGet("GetSecondMagicStruct/{id}")]
        public async Task<ActionResult<SecondMagicStruct>> GetSecondMagicStruct(int id)
        {
            return Ok(await _queryService.GetSecondMagicStruct(id));
        }

        [HttpGet("GetFirstMagicStruct/{id}")]
        public async Task<ActionResult<FirstMagicStruct>> GetFirstMagicStruct(int id)
        {
            return Ok(await _queryService.GetFirstMagicStruct(id));
        }

        [HttpGet("GetUserFinishedInCurrentYearTasks/{id}")]
        public async Task<ActionResult<List<TaskName>>> GetUserFinishedInCurrentYearTasks(int id)
        {
            return Ok(await _queryService.GetUserFinishedInCurrentYearTasks(id));
        }

        [HttpGet("GetUserTasksWithNameLessThanFortyFiveChars/{id}")]
        public async Task<ActionResult<List<Core.Entity.Task>>> GetUserTasksWithNameLessThanFortyFiveChars(int id)
        {
            return Ok(await _queryService.GetUserTasksWithNameLessThanFortyFiveChars(id));
        }

        [HttpGet("GetDataHierarchically")]
        public async Task<ActionResult<List<ProjectInfo>>> GetDataHierarchically()
        {
            return Ok(await _queryService.GetDataHierarchically());
        }

        [HttpGet("GetTeamsWithUsersOlderThanTwelveYears")]
        public async Task<ActionResult<List<TeamWithUsers>>> GetTeamsWithUsersOlderThanTwelveYears()
        {
            return Ok(await _queryService.GetTeamsWithUsersOlderThanTwelveYears());
        }

        [HttpGet("GetSortedUsersWithTasks")]
        public async Task<ActionResult<List<UserWithTasks>>> GetSortedUsersWithTasks()
        {
            return Ok(await _queryService.GetSortedUsersWithTasks());
        }
    }
}
