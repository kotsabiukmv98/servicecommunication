﻿using Microsoft.AspNetCore.Mvc;
using ServiceCommunication.Core.ApplicationService.Interfaces;
using System.Threading.Tasks;

namespace ServiceCommunication.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LogsController : ControllerBase
    {
        private readonly ILogService _logService;
        public LogsController(ILogService logService)
        {
            _logService = logService;
        }

        // GET: api/Log
        [HttpGet]
        public async Task<ActionResult> Get()
        {
            await _logService.SendLogs();
            return NoContent();
        }
    }
}
