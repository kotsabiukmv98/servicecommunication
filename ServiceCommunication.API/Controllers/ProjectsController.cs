﻿using Microsoft.AspNetCore.Mvc;
using ServiceCommunication.Core.ApplicationService.Interfaces;
using ServiceCommunication.Core.ApplicationService.Models;
using ServiceCommunication.Core.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ServiceCommunication.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectService _projectService;

        public ProjectsController(IProjectService projectService,
                                  IQueueService queueService)
        {
            _projectService = projectService;
        }

        // GET: api/Projects
        [HttpGet]
        public async Task<ActionResult<ICollection<Project>>> Get()
        {
            return Ok(await _projectService.GetAllProjects());
        }

        // GET: api/Projects/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Project>> Get(int id)
        {
            var project = await _projectService.FindProjectById(id);
            if (project == null)
            {
                return StatusCode(404, $"Did not find project with ID {id}");
            }
            return project;
        }

        // POST: api/Projects
        [HttpPost]
        public async Task<ActionResult<Project>> Post([FromBody] NewProject project)
        {
            if (string.IsNullOrEmpty(project.Name))
            {
                return BadRequest("Name is required for creating Project");
            }
            return await _projectService.CreateProject(project);
        }

        // PUT: api/Projects/5
        [HttpPut("{id}")]
        public async Task<ActionResult<Project>> Put(int id, [FromBody] NewProject newProject)
        {
            var project = await _projectService.UpdateProject(id, newProject);
            if (newProject == null)
            {
                return StatusCode(404, $"Did not find project with ID {id}");
            }
            return project;
        }

        // DELETE: api/Projects/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await _projectService.DeleteProject(id);
            return NoContent();
        }
    }
}
