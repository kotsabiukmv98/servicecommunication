﻿using Microsoft.AspNetCore.Mvc;
using ServiceCommunication.Core.ApplicationService.Interfaces;
using ServiceCommunication.Core.ApplicationService.Models;
using ServiceCommunication.Core.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace ServiceCommunication.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        // GET: api/Users
        [HttpGet]
        public async Task<ActionResult<ICollection<User>>> Get()
        {
            return Ok(await _userService.GetAllUsers());
        }

        // GET: api/Users/5
        [HttpGet("{id}")]
        public async Task<ActionResult<User>> Get(int id)
        {
            var user = await _userService.FindUserById(id);
            if (user == null)
            {
                return StatusCode(404, $"Did not find user with ID {id}");
            }
            return Ok(user);
        }

        // POST: api/Users
        [HttpPost]
        public async Task<ActionResult<User>> Post([FromBody] NewUser user)
        {
            if (string.IsNullOrEmpty(user.FirstName))
            {
                return BadRequest("FirstName is required for creating User");
            }
            if (string.IsNullOrEmpty(user.LastName))
            {
                return BadRequest("LastName is required for creating User");
            }
            if (string.IsNullOrEmpty(user.Email))
            {
                return BadRequest("Email is required for creating User");
            }

            return Ok(await _userService.CreateUser(user));
        }

        // PUT: api/Users/5
        [HttpPut("{id}")]
        public async Task<ActionResult<User>> Put(int id, [FromBody] NewUser newUser)
        {
            var user = await _userService.UpdateUser(id, newUser);
            if (user == null)
            {
                return StatusCode(404, $"Did not find user with ID {id}");
            }

            return Ok(user);
        }

        // DELETE: api/Users/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await _userService.DeleteUser(id);
            return NoContent();
        }
    }
}
